cmake_minimum_required(VERSION 3.1.0)
project(ceres_dtn)

find_package(Ceres REQUIRED)
include_directories(${CERES_INCLUDE_DIRS})

add_subdirectory(pybind11)
pybind11_add_module(ceres_dtn src/full.cpp)

target_link_libraries(ceres_dtn PRIVATE ${CERES_LIBRARIES})
