// Ceres Solver - A fast non-linear least squares minimizer
// Copyright 2015 Google Inc. All rights reserved.
// http://ceres-solver.org/
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
// * Neither the name of Google Inc. nor the names of its contributors may be
//   used to endorse or promote products derived from this software without
//   specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Author: keir@google.com (Keir Mierle)
//
// A simple example of using the Ceres minimizer.
//
// Minimize 0.5 (10 - x)^2 using jacobian matrix computed using
// automatic differentiation.

#include <memory>
#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/iostream.h>

//#include "min_dtn.hpp" 
#include "Eigen/Core"
// #include <vector>
// #include "Eigen/StdVector"
#include "ceres/ceres.h"
#include "ceres/loss_function.h"
#include "glog/logging.h"
#include <stdexcept>

// std
using std::shared_ptr;
// ceres 
using ceres::AutoDiffCostFunction;
using ceres::CostFunction;
using ceres::Problem;
using ceres::Solver;
using ceres::Solve;
using ceres::ScaledLoss;
using ceres::TAKE_OWNERSHIP;
// Eigen
using Eigen::Dynamic;
using Eigen::RowMajor;
typedef std::complex<double> Complex;
typedef Eigen::Matrix<Complex,Dynamic,1> ComplexVector;
typedef Eigen::Matrix<double,Dynamic,1> RealVector;
typedef Eigen::Matrix<Complex,Dynamic,Dynamic,RowMajor> ComplexMatrix;
typedef Eigen::Matrix<double,Dynamic,Dynamic,RowMajor> RealMatrix;
typedef Eigen::Matrix< std::complex<const double> ,Dynamic,Dynamic,RowMajor> ConstComplexMatrix;



struct CostFunctor {
  template <typename T> bool operator()(const T* const x, T* residual) const {
    
    Eigen::Matrix<T,1,1> A; 
    // RealMatrix A(1,1);
    A(0,0)= x[0]; 
    // residual[0] = 10.0 - x[0];
    residual[0] = 10.0 - A(0,0);
    return true;
  }
}; 


void SetSolverOptions( Solver::Options &options,pybind11::dict flags) {
  
  // setting some default values 
  options.max_num_iterations = 50000;
  options.linear_solver_type = ceres::DENSE_QR;
  options.use_nonmonotonic_steps = true;
  options.minimizer_progress_to_stdout = true;
  
  // user-defined options
  for(auto item : flags) {
    //std::cout << "key = " << std::string(pybind11::str(item.first))
    //          << ",value = " << std::string(pybind11::str(item.second)) << std::endl; 
    auto key = std::string(pybind11::str(item.first)); 
    if ( key == "max_num_iterations") {
      options.max_num_iterations = pybind11::cast<int>(item.second);
    }
    if ( key == "check_gradients") {
      options.check_gradients  = pybind11::cast<bool>(item.second);
    }
    if ( key == "use_nonmonotonic_steps") {
      options.use_nonmonotonic_steps = pybind11::cast<bool>(item.second);
    }
    if ( key == "minimizer_progress_to_stdout") {
      options.minimizer_progress_to_stdout = pybind11::cast<bool>(item.second);
    }
    if ( key == "num_threads") {
      options.num_threads = pybind11::cast<int>(item.second);
    }
    if ( key == "parameter_tolerance") {
      options.parameter_tolerance = pybind11::cast<double>(item.second);
    }
    if ( key == "function_tolerance") {
      options.function_tolerance = pybind11::cast<double>(item.second);
    }
    if ( key == "gradient_tolerance") {
      options.gradient_tolerance = pybind11::cast<double>(item.second);
    }
    if ( key == "max_solver_time_in_seconds") {
      options.max_solver_time_in_seconds = pybind11::cast<double>(item.second);
    }
    if ( key == "max_consecutive_nonmonotonic_steps") {
      options.max_consecutive_nonmonotonic_steps = pybind11::cast<int>(item.second);
    }
    if ( key == "max_num_consecutive_invalid_steps") {
      options.max_num_consecutive_invalid_steps = pybind11::cast<int>(item.second);
    }
    if ( key == "initial_trust_region_radius") {
      options.initial_trust_region_radius = pybind11::cast<double>(item.second);
    }
    if ( key == "max_trust_region_radius") {
      options.max_trust_region_radius = pybind11::cast<double>(item.second);
    }
    if ( key == "min_relative_decrease") {
      options.min_relative_decrease = pybind11::cast<double>(item.second);
    }
    if ( key == "min_lm_diagonal") {
      options.min_lm_diagonal = pybind11::cast<double>(item.second);
    }
    if ( key == "max_lm_diagonal") {
      options.max_lm_diagonal = pybind11::cast<double>(item.second);
    }
    if ( key == "use_inner_iterations") {
      options.use_inner_iterations = pybind11::cast<bool>(item.second);
    }
    if ( key == "trust_region_strategy_type" ) {	
      auto trust_strat = std::string(pybind11::str(item.second));
      if (trust_strat == "LEVENBERG_MARQUARDT") {
        options.trust_region_strategy_type = ceres::LEVENBERG_MARQUARDT;
      }
      else {
        if (trust_strat == "DOGLEG") {
          options.trust_region_strategy_type = ceres::DOGLEG;
        }
        else {
	  std::cout << "I do not know the trust_region_strategy " << trust_strat << std::endl;
	  std::cout << "Valid options are: LEVENBERG_MARQUARDT and DOGLEG" << std::endl;
        }
      }
    }  
    if ( key == "dogleg_type" ) {	
      auto dogleg_choice = std::string(pybind11::str(item.second));
      if (dogleg_choice == "TRADITIONAL_DOGLEG") {
        options.dogleg_type = ceres::TRADITIONAL_DOGLEG;
      }
      else {
        if (dogleg_choice == "SUBSPACE_DOGLEG") {
          options.dogleg_type = ceres::SUBSPACE_DOGLEG;
        }
        else {
	  std::cout << "I do not know the dogleg_strategy " << dogleg_choice << std::endl;
	  std::cout << "Valid options are: TRADITIONAL_DOGLEG and SUBSPACE_DOGLEG" << std::endl;
        }
      }
    }  
  }

}

class BoolIterationCallback : public ceres::IterationCallback {
  public:
    double sigma_tol = 1e-10; 
    double * smallest_sigma = NULL; 
    pybind11::list * sigma_iter = NULL; 
    pybind11::list * cost_iter = NULL; 

    BoolIterationCallback( double a_sigma_tol,double* a_smallest_sigma, pybind11::list * a_sigma_iter, pybind11::list * a_cost_iter ) {
      sigma_tol = a_sigma_tol; 
      smallest_sigma = a_smallest_sigma; 
      sigma_iter = a_sigma_iter; 
      cost_iter = a_cost_iter; 
    }
    ~BoolIterationCallback() {}
    void SetSingTol(double new_tol) {
      sigma_tol = new_tol;
    }
    ceres::CallbackReturnType operator()(const ceres::IterationSummary& summary) {
      sigma_iter->append(*smallest_sigma);
      cost_iter->append(summary.cost); 
      if (smallest_sigma != NULL && (*smallest_sigma) < sigma_tol ) {
        std::cout << "smallest_sigma = " << *smallest_sigma << std::endl;  
        return ceres::SOLVER_ABORT;
      }
      else {
        return ceres::SOLVER_CONTINUE;
      }
    }
};

template <typename Rhs> 
Rhs Solve_weighted(const Eigen::MatrixBase<Rhs> & b, 
		   Eigen::ColPivHouseholderQR<ComplexMatrix> AH_QR,
                   Eigen::HouseholderQR<ComplexMatrix> RH_QR) {
  	
  RealMatrix P = AH_QR.colsPermutation();  

  Rhs tmp = P.transpose() * b;
  Rhs sol_lq = RH_QR.solve(tmp);
   
  return AH_QR.householderQ() * sol_lq;
  
  //return b;
}



Complex Maximize_by_sampling(ComplexVector b, ComplexMatrix A, RealVector lam, RealVector weight,
		             pybind11::dict flags, Eigen::Ref<RealMatrix> obj_fun) {

  // Eigen::Ref<RealMatrix> obj_fun = pybind11::cast<Eigen::Ref<RealMatrix>>(flags["obj_fun"][-1]);  
  Eigen::Ref<RealVector> x_sample = pybind11::cast<Eigen::Ref<RealVector>>(flags["x_sample"]); 
  Eigen::Ref<RealVector> y_sample = pybind11::cast<Eigen::Ref<RealVector>>(flags["y_sample"]); 

  // std::cout << "obj_fun = " << obj_fun << std::endl;
  // obj_fun(0,0) = Complex(99.9,0);   
  //
  // double xmin = 10;
  // double ymin = 10; 
  // double xmax = 1000; 
  // double ymax = 1000; 
  int Nx = x_sample.rows();
  int Ny = y_sample.rows(); 
  // double hx = (xmax-xmin)/(Nx-1);
  // double hy = (ymax-ymin)/(Ny-1); 

  ComplexVector Z(Nx*Ny);
  RealVector vals(Nx*Ny);
  
  ComplexVector az(lam.size());
 
  ComplexMatrix A_prev(A.rows(),A.cols());
  for(int k=0; k < A.rows(); k++) {
    for(int m = 0; m < A.cols(); m++){
      A_prev(k,m) = A(k,m);
    }
  }
  auto A_prev_QR = A_prev.completeOrthogonalDecomposition();
  ComplexVector res = A_prev * A_prev_QR.solve(b) - b; 
   
  
  std::cout << "res = " << res << std::endl;
  int i_r; 
  double max_res =  res.array().abs().maxCoeff(&i_r);        
  std::cout << "max_res = " << max_res << "i_r = " << i_r  << std::endl;
  

  for(int k = 0; k < Ny; k++) {
    // std::cout << "k = " << k << std::endl;
    for(int m = 0; m < Nx; m++) {
      
      Z(k+m*Nx) = Complex(x_sample(k),y_sample(m));

      /*if (k == 0 && m == 0) {
        Z(k+m*Nx) = Complex(928.4175,1197.80336);
      }*/
 
        
      for (int l=0; l < lam.size(); l++) {
        az(l) = -weight(l)/(Z(k+m*Nx)  + lam(l));
	// A(l,A.cols()-1) = az(l);  
      } 

        // auto A_QR = A.completeOrthogonalDecomposition();
        // ComplexVector res = A * A_QR.solve(b) - b;
        // int i_r; 
	// double max_res =  res.array().abs().maxCoeff(&i_r);        
	// std::cout << "max_res = " << max_res << "i_r = " << i_r  << std::endl;
        
      Complex tmp = az.dot(res);
      Complex tmp2 = az.dot(az);
      
      vals(k+m*Nx) =  (std::conj(tmp)*tmp).real() / tmp2.real();
      obj_fun(m,k) = vals(k+m*Nx) / std::pow(res.norm(),2);
       
       /* 
      std::cout << "vals(" << k+m*Nx << ") = " << vals(k+m*Nx) 
	        << ",tmp =" << tmp 
	        << ",tmp2 = " << tmp2 
		<< ",(std::conj(tmp)*tmp) = " <<  (std::conj(tmp)*tmp)
	        <<  std::endl;
      */
     }
  }

  int idx_max; 
  double max_val = vals.maxCoeff(&idx_max);
  
  for (int l=0; l < lam.size(); l++) {
    A(l,A.cols()-1) =  -weight(l)/(Z(idx_max)  + lam(l));
  }
  
  auto A_QR = A.completeOrthogonalDecomposition();
  ComplexVector resi = A * A_QR.solve(b) - b; 
  
   std::cout
	    << ",z_max = " << Z(idx_max) 
	    << ", max_val = " << max_val 
	    << ", sqrt(max_val) = " << std::sqrt(max_val)
	    << ", res.norm() = " << std::pow(resi.norm(),2)
	    << std::endl; 
  return Z(idx_max); 

}


Complex Maximize_by_sampling_LQ(ComplexVector b, ComplexMatrix A, RealVector lam, RealVector weight,
		                pybind11::dict flags, Eigen::Ref<RealMatrix> obj_fun ) {

  std::cout << "Hello from Maximize_by_sampling_LQ" << std::endl; 
  // Eigen::Ref<RealMatrix> obj_fun = pybind11::cast<Eigen::Ref<RealMatrix>>(flags["obj_fun"][-1]);  
  Eigen::Ref<RealVector> x_sample = pybind11::cast<Eigen::Ref<RealVector>>(flags["x_sample"]); 
  Eigen::Ref<RealVector> y_sample = pybind11::cast<Eigen::Ref<RealVector>>(flags["y_sample"]); 

  int Nx = x_sample.rows();
  int Ny = y_sample.rows(); 

  ComplexVector Z(Nx*Ny);
  RealVector vals(Nx*Ny);
  
  ComplexVector az(lam.size());
 
  ComplexMatrix A_prev(A.rows(),A.cols());
  for(int k=0; k < A.rows(); k++) {
    for(int m = 0; m < A.cols(); m++){
      A_prev(k,m) = A(k,m);
    }
  }
  auto A_prev_QR = A_prev.completeOrthogonalDecomposition();
  ComplexVector res = A_prev * A_prev_QR.solve(b) - b; 
   
  ComplexMatrix A_augm(A.rows(),A.cols()+1);
  // ComplexMatrix A_augm(A.rows(),A.cols());
  for(int k=0; k < A.rows(); k++) {
    for(int m = 0; m < A.cols(); m++){
      A_augm(k,m) = A(k,m);
    }
  }
  
  double res_val = std::pow(res.norm(),2);

  for(int k = 0; k < Ny; k++) {
    for(int m = 0; m < Nx; m++) {
      // std::cout << "k = " << k << ", m = " << m << std::endl;
      Z(k+m*Nx) = Complex(x_sample(k),y_sample(m));
        
      for (int l=0; l < lam.size(); l++) {
        az(l) = -weight(l)/(Z(k+m*Nx)  + lam(l));
	A_augm(l,A_augm.cols()-1) = az(l);  
      } 

        auto A_augm_QR = A_augm.completeOrthogonalDecomposition();
        ComplexVector new_weights = A_augm_QR.solve(res);
         
      // Complex tmp = az.dot(res);
      // Complex tmp2 = az.dot(az);
      
      vals(k+m*Nx) =  res_val - std::pow((A_augm*new_weights-res).norm(),2);
      
      //vals(k+m*Nx) =  (std::conj(tmp)*tmp).real() / tmp2.real();
      obj_fun(m,k) = vals(k+m*Nx) / res_val;
       
     }
  }

  int idx_max; 
  double max_val = vals.maxCoeff(&idx_max);
  
  for (int l=0; l < lam.size(); l++) {
    A(l,A.cols()-1) =  -weight(l)/(Z(idx_max)  + lam(l));
  }
  
  auto A_QR = A.completeOrthogonalDecomposition();
  ComplexVector resi = A * A_QR.solve(b) - b; 
  
   std::cout
	    << ",z_max = " << Z(idx_max) 
	    << ", max_val = " << max_val 
	    << ", sqrt(max_val) = " << std::sqrt(max_val)
	    << ", res.norm() = " << std::pow(resi.norm(),2)
	    << std::endl; 
  return Z(idx_max); 

}

Complex Maximize_by_sampling_direct(ComplexVector b, ComplexMatrix A, RealVector lam, RealVector weight,
		pybind11::dict flags, Eigen::Ref<RealMatrix> obj_fun) {

  std::cout << "Hello from Maximize_by_sampling_direct" << std::endl; 
  // Eigen::Ref<RealMatrix> obj_fun = pybind11::cast<Eigen::Ref<RealMatrix>>(flags["obj_fun"][-1]);  
  Eigen::Ref<RealVector> x_sample = pybind11::cast<Eigen::Ref<RealVector>>(flags["x_sample"]); 
  Eigen::Ref<RealVector> y_sample = pybind11::cast<Eigen::Ref<RealVector>>(flags["y_sample"]); 

  int Nx = x_sample.rows();
  int Ny = y_sample.rows(); 

  ComplexVector Z(Nx*Ny);
  RealVector vals(Nx*Ny);
  
  ComplexVector az(lam.size());
 
  RealVector az_norm(Nx*Ny);
  
  ComplexMatrix A_prev(A.rows(),A.cols());
  for(int k=0; k < A.rows(); k++) {
    for(int m = 0; m < A.cols(); m++){
      A_prev(k,m) = A(k,m);
    }
  }
  auto A_prev_QR = A_prev.completeOrthogonalDecomposition();
  ComplexVector res = A_prev * A_prev_QR.solve(b) - b; 
    
  
  double res_val = std::pow(res.norm(),2);

  for(int k = 0; k < Ny; k++) {
    for(int m = 0; m < Nx; m++) {
    
      Z(k+m*Nx) = Complex(x_sample(k),y_sample(m));
        
      for (int l=0; l < lam.size(); l++) {
        az(l) = -weight(l)/(Z(k+m*Nx)  + lam(l));
      } 

        ComplexVector az_mod = A*A_prev_QR.solve(az) - az;
        // auto A_augm_QR = A_augm.completeOrthogonalDecomposition();
        // ComplexVector new_weights = A_augm_QR.solve(res);

        Complex tmp = az_mod.dot(res);
        Complex tmp2 = az_mod.dot(az_mod);
      
      az_norm(k+m*Nx) =  az_mod.norm();
      
      vals(k+m*Nx) =  (std::conj(tmp)*tmp).real() / tmp2.real();
      obj_fun(m,k) = vals(k+m*Nx) / res_val;
       
     }
  }

  int idx_max; 
  double max_val = vals.maxCoeff(&idx_max);
  double az_idx_norm = az_norm(idx_max); 
  
   
  for (int l=0; l < lam.size(); l++) {
    A(l,A.cols()-1) =  -weight(l)/(Z(idx_max)  + lam(l));
  }
  
  auto A_QR = A.completeOrthogonalDecomposition();
  ComplexVector resi = A * A_QR.solve(b) - b; 
  
   std::cout
	    << ",z_max = " << Z(idx_max) 
	    << ", max_val = " << max_val 
	    << ", sqrt(max_val) = " << std::sqrt(max_val)
	    << ", res.norm() = " << std::pow(resi.norm(),2)
	    << ", az_idx.norm() " << az_idx_norm 
	    << std::endl; 
  return Z(idx_max); 

}



class DtnCostFunction_full : public ceres::CostFunction {
  public:
    int N;
    Complex zeta;
    double lam; 

    DtnCostFunction_full(int aN,double alam,Complex azeta) {
      N = aN;
      lam = alam;
      zeta = azeta;
      set_num_residuals(2);
      // std::cout << "lam  = " << lam << ", zeta = " << zeta << std::endl;
      for(int i = 0; i < 4; i++){
        mutable_parameter_block_sizes()->push_back(N*N);
      }
    }
    virtual ~DtnCostFunction_full() {}
    virtual bool Evaluate(double const* const* parameters,
		          double* residuals,
			  double** jacobians) const {
      
      
      ComplexMatrix S(N,N); 
      ComplexMatrix SE;
      ComplexMatrix SE_inv;
      
      if(N > 1) { 
        SE.resize(N-1,N-1);
      }
      

      for (int i = 0; i < N; i++){
        for(int j=  0; j < N; j++){
          S(i,j) = Complex(parameters[0][j+i*N],parameters[1][j+i*N]) + lam*Complex(parameters[2][j+i*N],parameters[3][j+i*N]);
	  if (N > 1 && i > 0 && j > 0) {
	    SE(i-1,j-1) = S(i,j);
	  }
	}
      }

      
      if( N > 1) {
	// std::cout << "SE = " << SE << std::endl;
        SE_inv = SE.inverse();
	// std::cout << "SE_inv = " << SE_inv << std::endl;
      }

      Complex zeta_approx = S(0,0);
      if (N > 1) {
        for (int i=1;i<N;i++){
          for(int j=1;j<N;j++){
            zeta_approx -= S(0,i)*SE_inv(i-1,j-1)*S(j,0);
	  }
	}
      }
      Complex diff = (zeta-zeta_approx);

      residuals[0] = diff.real();
      // std::cout << "residuals[0] = " << residuals[0] << std::endl;
      residuals[1] = diff.imag();
      
      
      // residuals[0] = 10 - L1_real;
      // residuals[1] = 5 - L1_imag;

      if (jacobians != NULL) {
       
        ComplexMatrix D_SE_inv;
        for (int k = 0; k < N; k++) {
          for(int m = 0; m < N; m++) {
            Complex d_zeta_app_km = Complex(0,0);

            if ( k > 0 && m > 0) { 
              D_SE_inv.resize(N-1,N-1);
	      for(int i = 0; i < N-1; i++) {
	        for(int j = 0; j < N-1; j++) {
	          D_SE_inv(i,j) = -SE_inv(i,k-1)*SE_inv(m-1,j);
	        }
	      }
	    }


	    if (k==0 && m ==0) {
              d_zeta_app_km = Complex(1,0);
	    }
            if (k >= 1 && m == 0) {
	      for(int i=1; i < N; i++) {
	        d_zeta_app_km -= S(0,i)*SE_inv(i-1,k-1);
	      }
	    } 
            if (k == 0 && m >= 1) {
	      for(int j=1; j < N; j++) {
	        d_zeta_app_km -= S(j,0)*SE_inv(m-1,j-1);
	      }
	    }
	    if ( k >=1 && m >= 1) { 
	      for(int i = 1; i < N; i++) {
	        for(int j = 1; j < N; j++) {
	          d_zeta_app_km -= S(0,i)*D_SE_inv(i-1,j-1)*S(j,0);
	        }
	      }
	    }

            if (jacobians[0] != NULL) {
              jacobians[0][m+k*N] = -d_zeta_app_km.real();
              jacobians[0][m+k*N+N*N] = -d_zeta_app_km.imag();
            }
            if (jacobians[1] != NULL) {
              jacobians[1][m+k*N] = -(Complex(0,1)*d_zeta_app_km).real();
              jacobians[1][m+k*N+N*N] = -(Complex(0,1)*d_zeta_app_km).imag();
            }
            if (jacobians[2] != NULL) {
              jacobians[2][m+k*N] = -(lam*d_zeta_app_km).real();
              jacobians[2][m+k*N+N*N] = -(lam*d_zeta_app_km).imag();
            }
            if (jacobians[3] != NULL) {
              jacobians[3][m+k*N] = -(Complex(0,1)*lam*d_zeta_app_km).real();
              jacobians[3][m+k*N+N*N] = -(Complex(0,1)*lam*d_zeta_app_km).imag();
            }

	  }
        }
         

      }

      return true;
    }
};


class DtnCostFunction_medium : public ceres::CostFunction {
  public:
    int N;
    Complex zeta;
    double lam; 

    DtnCostFunction_medium(int aN,double alam,Complex azeta) {
      N = aN;
      lam = alam;
      zeta = azeta;
      set_num_residuals(2); // Real and Imaginary part
      mutable_parameter_block_sizes()->push_back(4); // II-block
      if ( N > 1) { // IE,EI and EE-blocks
        for (int j=0; j < 3; j++) {
          mutable_parameter_block_sizes()->push_back(4*(N-1)); 
        }
      }
    }
    virtual ~DtnCostFunction_medium() {}
    virtual bool Evaluate(double const* const* parameters,
		          double* residuals,
			  double** jacobians) const {
      
     Complex zeta_approx = Complex(parameters[0][0],parameters[0][1]) + lam*Complex(parameters[0][2],parameters[0][3]);
      
      if (N > 1) {
        for(int j=0;j<N-1;j++){
          Complex S_IE_j = Complex(parameters[1][j],parameters[1][j+N-1]) + lam*Complex(parameters[1][j+2*(N-1)],parameters[1][j+3*(N-1)]);
          Complex S_EI_j = Complex(parameters[2][j],parameters[2][j+N-1]) + lam*Complex(parameters[2][j+2*(N-1)],parameters[2][j+3*(N-1)]);
          Complex S_EE_j = Complex(parameters[3][j],parameters[3][j+N-1]) + lam*Complex(parameters[3][j+2*(N-1)],parameters[3][j+3*(N-1)]);
          zeta_approx -= S_IE_j*S_EI_j/S_EE_j;
	}	
      }

      Complex diff = (zeta-zeta_approx);
      residuals[0] = diff.real();
      // std::cout << "residuals[0] = " << residuals[0] << std::endl;
      residuals[1] = diff.imag();
            
      
      if (jacobians != NULL) {

	// II-block
        if (jacobians[0] != NULL) {
	  jacobians[0][0] = -1; // real part 
	  jacobians[0][0+4] = 0; // imaginary part
	  jacobians[0][1] = 0;
	  jacobians[0][1+4] = -1;
	  jacobians[0][2] = -lam;
	  jacobians[0][2+4] = 0;
	  jacobians[0][3] = 0;
	  jacobians[0][3+4] = -lam;
        }

        int offset = 4*(N-1); // for residual[1]

	for (int j =0; j < N-1; j++) {
          
	  Complex S_IE_j = Complex(parameters[1][j],parameters[1][j+N-1]) + lam*Complex(parameters[1][j+2*(N-1)],parameters[1][j+3*(N-1)]);
          Complex S_EI_j = Complex(parameters[2][j],parameters[2][j+N-1]) + lam*Complex(parameters[2][j+2*(N-1)],parameters[2][j+3*(N-1)]);
          Complex S_EE_j = Complex(parameters[3][j],parameters[3][j+N-1]) + lam*Complex(parameters[3][j+2*(N-1)],parameters[3][j+3*(N-1)]);
	  
	  Complex d_L1_IE_real_j = (S_EI_j/S_EE_j); 
	  Complex d_L1_EI_real_j = (S_IE_j/S_EE_j);
	  Complex d_L1_EE_real_j = -(S_IE_j*S_EI_j/(S_EE_j*S_EE_j));

	  // IE-block
          if (jacobians[1] != NULL) {

	    jacobians[1][j] = d_L1_IE_real_j.real(); 
	    jacobians[1][j+offset] = d_L1_IE_real_j.imag(); 

	    jacobians[1][j+(N-1)] = -d_L1_IE_real_j.imag(); 
	    jacobians[1][j+(N-1)+offset] = d_L1_IE_real_j.real(); 
	    
	    jacobians[1][j+2*(N-1)] = lam*d_L1_IE_real_j.real(); 
	    jacobians[1][j+2*(N-1)+offset] = lam*d_L1_IE_real_j.imag(); 
	    
	    jacobians[1][j+3*(N-1)] = -lam*d_L1_IE_real_j.imag(); 
	    jacobians[1][j+3*(N-1)+offset] = lam*d_L1_IE_real_j.real(); 
          }  

	  // EI-block
          if (jacobians[2] != NULL) {
	    jacobians[2][j] = d_L1_EI_real_j.real(); 
	    jacobians[2][j+offset] = d_L1_EI_real_j.imag();
	    
	    jacobians[2][j+(N-1)] = -d_L1_EI_real_j.imag(); 
	    jacobians[2][j+(N-1)+offset] = d_L1_EI_real_j.real(); 
	    
	    jacobians[2][j+2*(N-1)] = lam*d_L1_EI_real_j.real(); 
	    jacobians[2][j+2*(N-1)+offset] = lam*d_L1_EI_real_j.imag(); 
	    
	    jacobians[2][j+3*(N-1)] = -lam*d_L1_EI_real_j.imag(); 
	    jacobians[2][j+3*(N-1)+offset] = lam*d_L1_EI_real_j.real(); 
          }  
	
	  // EE-block
          if (jacobians[3] != NULL) {
	    jacobians[3][j] = d_L1_EE_real_j.real(); 
	    jacobians[3][j+offset] = d_L1_EE_real_j.imag();

	    jacobians[3][j+(N-1)] = -d_L1_EE_real_j.imag(); 
	    jacobians[3][j+(N-1)+offset] = d_L1_EE_real_j.real(); 

	    jacobians[3][j+2*(N-1)] = lam*d_L1_EE_real_j.real(); 
	    jacobians[3][j+2*(N-1)+offset] = lam*d_L1_EE_real_j.imag(); 
	    
	    jacobians[3][j+3*(N-1)] = -lam*d_L1_EE_real_j.imag(); 
	    jacobians[3][j+3*(N-1)+offset] = lam*d_L1_EE_real_j.real(); 
          }  

	}
	
      }

      return true;
    }
};

class DtnCostFunction_minimalIC : public ceres::CostFunction {
  public:
    int N;
    Complex zeta;
    double lam; 

    DtnCostFunction_minimalIC(int aN,double alam,Complex azeta) {
      N = aN;
      lam = alam;
      zeta = azeta;
      set_num_residuals(2); // Real and Imaginary part
      mutable_parameter_block_sizes()->push_back(4); // II-block
      if (N > 1) {
        mutable_parameter_block_sizes()->push_back(4*(N-1)); // IE-block
        mutable_parameter_block_sizes()->push_back(2*(N-1)); // EI-block
        mutable_parameter_block_sizes()->push_back(2*(N-1)); // EE-block
      }
    }
    virtual ~DtnCostFunction_minimalIC() {}
    virtual bool Evaluate(double const* const* parameters,
		          double* residuals,
			  double** jacobians) const {
      
     Complex zeta_approx = Complex(parameters[0][0],parameters[0][1]) + lam*Complex(parameters[0][2],parameters[0][3]);
      
      if (N > 1) {
        for(int j=0;j<N-1;j++){
          Complex S_IE_j = Complex(parameters[1][j],parameters[1][j+N-1]) + lam*Complex(parameters[1][j+2*(N-1)],parameters[1][j+3*(N-1)]);
          Complex S_EI_j = Complex(parameters[2][j],parameters[2][j+N-1]) + lam;
          Complex S_EE_j = Complex(parameters[3][j],parameters[3][j+N-1]) + lam;
          zeta_approx -= S_IE_j*S_EI_j/S_EE_j;
	}	
      }

      Complex diff = (zeta-zeta_approx);
      residuals[0] = diff.real();
      // std::cout << "residuals[0] = " << residuals[0] << std::endl;
      residuals[1] = diff.imag();
            
      
      if (jacobians != NULL) {

	// II-block
        if (jacobians[0] != NULL) {
	  jacobians[0][0] = -1; // real part 
	  jacobians[0][0+4] = 0; // imaginary part
	  jacobians[0][1] = 0;
	  jacobians[0][1+4] = -1;
	  jacobians[0][2] = -lam;
	  jacobians[0][2+4] = 0;
	  jacobians[0][3] = 0;
	  jacobians[0][3+4] = -lam;
        }

        int offset1 = 4*(N-1); // for residual[1]
        int offset2 = 2*(N-1); // for residual[2],residual[3]

	for (int j =0; j < N-1; j++) {
          
	  Complex S_IE_j = Complex(parameters[1][j],parameters[1][j+N-1]) + lam*Complex(parameters[1][j+2*(N-1)],parameters[1][j+3*(N-1)]);
          Complex S_EI_j = Complex(parameters[2][j],parameters[2][j+N-1]) + lam;
          Complex S_EE_j = Complex(parameters[3][j],parameters[3][j+N-1]) + lam;
	  
	  Complex d_L1_IE_real_j = (S_EI_j/S_EE_j); 
	  Complex d_L1_EI_real_j = (S_IE_j/S_EE_j);
	  Complex d_L1_EE_real_j = -(S_IE_j*S_EI_j/(S_EE_j*S_EE_j));

	  // IE-block
          if (jacobians[1] != NULL) {

	    jacobians[1][j] = d_L1_IE_real_j.real(); 
	    jacobians[1][j+offset1] = d_L1_IE_real_j.imag(); 

	    jacobians[1][j+(N-1)] = -d_L1_IE_real_j.imag(); 
	    jacobians[1][j+(N-1)+offset1] = d_L1_IE_real_j.real(); 
	    
	    jacobians[1][j+2*(N-1)] = lam*d_L1_IE_real_j.real(); 
	    jacobians[1][j+2*(N-1)+offset1] = lam*d_L1_IE_real_j.imag(); 
	    
	    jacobians[1][j+3*(N-1)] = -lam*d_L1_IE_real_j.imag(); 
	    jacobians[1][j+3*(N-1)+offset1] = lam*d_L1_IE_real_j.real(); 
          }  

	  // EI-block
          if (jacobians[2] != NULL) {
	    jacobians[2][j] = d_L1_EI_real_j.real(); 
	    jacobians[2][j+offset2] = d_L1_EI_real_j.imag();
	    
	    jacobians[2][j+(N-1)] = -d_L1_EI_real_j.imag(); 
	    jacobians[2][j+(N-1)+offset2] = d_L1_EI_real_j.real(); 
	    
          }  
	
	  // EE-block
          if (jacobians[3] != NULL) {
	    jacobians[3][j] = d_L1_EE_real_j.real(); 
	    jacobians[3][j+offset2] = d_L1_EE_real_j.imag();

	    jacobians[3][j+(N-1)] = -d_L1_EE_real_j.imag(); 
	    jacobians[3][j+(N-1)+offset2] = d_L1_EE_real_j.real(); 

          }  

	}
	
      }

      return true;
    }
};

class DtnCostFunction_mediumSym : public ceres::CostFunction {
  public:
    int N;
    Complex zeta;
    double lam; 

    DtnCostFunction_mediumSym(int aN,double alam,Complex azeta) {
      N = aN;
      lam = alam;
      zeta = azeta;
      set_num_residuals(2); // Real and Imaginary part
      mutable_parameter_block_sizes()->push_back(4); // II-block
      if (N > 1) {
        mutable_parameter_block_sizes()->push_back(4*(N-1)); // IE-block
        mutable_parameter_block_sizes()->push_back(2*(N-1)); // EE-block
      }
    }
    virtual ~DtnCostFunction_mediumSym() {}
    virtual bool Evaluate(double const* const* parameters,
		          double* residuals,
			  double** jacobians) const {
      
     Complex zeta_approx = Complex(parameters[0][0],parameters[0][1]) + lam*Complex(parameters[0][2],parameters[0][3]);
      
      if (N > 1) {
        for(int j=0;j<N-1;j++){
          Complex S_IE_j = Complex(parameters[1][j],parameters[1][j+N-1]) + lam*Complex(parameters[1][j+2*(N-1)],parameters[1][j+3*(N-1)]);
          Complex S_EE_j = Complex(parameters[2][j],parameters[2][j+N-1]) + lam;
          zeta_approx -= S_IE_j*S_IE_j/S_EE_j;
	}	
      }

      Complex diff = (zeta-zeta_approx);
      residuals[0] = diff.real();
      // std::cout << "residuals[0] = " << residuals[0] << std::endl;
      residuals[1] = diff.imag();
            
      
      if (jacobians != NULL) {

	// II-block
        if (jacobians[0] != NULL) {
	  jacobians[0][0] = -1; // real part 
	  jacobians[0][0+4] = 0; // imaginary part
	  jacobians[0][1] = 0;
	  jacobians[0][1+4] = -1;
	  jacobians[0][2] = -lam;
	  jacobians[0][2+4] = 0;
	  jacobians[0][3] = 0;
	  jacobians[0][3+4] = -lam;
        }

        int offset1 = 4*(N-1); // for residual[1]
        int offset2 = 2*(N-1); // for residual[2]

	for (int j =0; j < N-1; j++) {
          
	  Complex S_IE_j = Complex(parameters[1][j],parameters[1][j+N-1]) + lam*Complex(parameters[1][j+2*(N-1)],parameters[1][j+3*(N-1)]);
          Complex S_EE_j = Complex(parameters[2][j],parameters[2][j+N-1]) + lam;
	  
	  Complex d_L1_IE_real_j = 2.0*(S_IE_j/S_EE_j); 
	  Complex d_L1_EE_real_j = -(S_IE_j*S_IE_j/(S_EE_j*S_EE_j));

	  // IE-block
          if (jacobians[1] != NULL) {

	    jacobians[1][j] = d_L1_IE_real_j.real(); 
	    jacobians[1][j+offset1] = d_L1_IE_real_j.imag(); 

	    jacobians[1][j+(N-1)] = -d_L1_IE_real_j.imag(); 
	    jacobians[1][j+(N-1)+offset1] = d_L1_IE_real_j.real(); 
	    
	    jacobians[1][j+2*(N-1)] = lam*d_L1_IE_real_j.real(); 
	    jacobians[1][j+2*(N-1)+offset1] = lam*d_L1_IE_real_j.imag(); 
	    
	    jacobians[1][j+3*(N-1)] = -lam*d_L1_IE_real_j.imag(); 
	    jacobians[1][j+3*(N-1)+offset1] = lam*d_L1_IE_real_j.real(); 
          }  
	
	  // EE-block
          if (jacobians[2] != NULL) {
	    jacobians[2][j] = d_L1_EE_real_j.real(); 
	    jacobians[2][j+offset2] = d_L1_EE_real_j.imag();

	    jacobians[2][j+(N-1)] = -d_L1_EE_real_j.imag(); 
	    jacobians[2][j+(N-1)+offset2] = d_L1_EE_real_j.real(); 

          }  

	}
	
      }

      return true;
    }
};

class DtnCostFunction_minimal : public ceres::CostFunction {
  public:
    int N;
    Complex zeta;
    double lam; 

    DtnCostFunction_minimal(int aN,double alam,Complex azeta) {
      N = aN;
      lam = alam;
      zeta = azeta;
      set_num_residuals(2); // Real and Imaginary part
      mutable_parameter_block_sizes()->push_back(4); // II-block
      if ( N > 1) {
	// IE-block
        mutable_parameter_block_sizes()->push_back(2*(N-1));
        // EE-block
	mutable_parameter_block_sizes()->push_back(2*(N-1));
      }
    }
    virtual ~DtnCostFunction_minimal() {}
    virtual bool Evaluate(double const* const* parameters,
		          double* residuals,
			  double** jacobians) const {
      
     Complex zeta_approx = Complex(parameters[0][0],parameters[0][1]) + lam*Complex(parameters[0][2],parameters[0][3]);
      
      if (N > 1) {
        for(int j=0;j<N-1;j++){
          Complex S_IE_squared_j = Complex(parameters[1][j],parameters[1][j+N-1]);
          Complex S_EE_j = Complex(parameters[2][j],parameters[2][j+N-1]) + lam;
          zeta_approx -= S_IE_squared_j/S_EE_j;
	}	
      }

      Complex diff = (zeta-zeta_approx);
      residuals[0] = diff.real();
      // std::cout << "residuals[0] = " << residuals[0] << std::endl;
      residuals[1] = diff.imag();
            
      
      if (jacobians != NULL) {

	// II-block
        if (jacobians[0] != NULL) {
	  jacobians[0][0] = -1; // real part 
	  jacobians[0][0+4] = 0; // imaginary part
	  jacobians[0][1] = 0;
	  jacobians[0][1+4] = -1;
	  jacobians[0][2] = -lam;
	  jacobians[0][2+4] = 0;
	  jacobians[0][3] = 0;
	  jacobians[0][3+4] = -lam;
        }

        int offset = 2*(N-1); // for residual[1]

	for (int j =0; j < N-1; j++) {
          
          Complex S_IE_squared_j = Complex(parameters[1][j],parameters[1][j+N-1]);
          Complex S_EE_j = Complex(parameters[2][j],parameters[2][j+N-1]) + lam;
	  
	  Complex d_L1_IE_real_j = 1.0/S_EE_j; 
	  Complex d_L1_EE_real_j = -(S_IE_squared_j/(S_EE_j*S_EE_j));

	  // IE-block
          if (jacobians[1] != NULL) {

	    jacobians[1][j] = d_L1_IE_real_j.real(); 
	    jacobians[1][j+offset] = d_L1_IE_real_j.imag(); 

	    jacobians[1][j+(N-1)] = -d_L1_IE_real_j.imag(); 
	    jacobians[1][j+(N-1)+offset] = d_L1_IE_real_j.real(); 
	    
          }  

	  // EE-block
          if (jacobians[2] != NULL) {
	    jacobians[2][j] = d_L1_EE_real_j.real(); 
	    jacobians[2][j+offset] = d_L1_EE_real_j.imag();

	    jacobians[2][j+(N-1)] = -d_L1_EE_real_j.imag(); 
	    jacobians[2][j+(N-1)+offset] = d_L1_EE_real_j.real(); 

          }  

	}
	
      }

      return true;
    }
};


class DtnCostFunction_reduced : public ceres::CostFunction {
  public:
    int N;
    int L;
    RealVector lam; // eigenvalues
    ComplexVector zeta_ref; // dtn numbers 
    RealVector weight; // weights
    std::string pseudo_type;
    double sing_tol = 8e-6;
    double* smallest_sigma = NULL; 

    DtnCostFunction_reduced(int aN, RealVector alam,ComplexVector azeta_ref,RealVector aweight, std::string apseudo_type) {
      N = aN;
      lam = alam;
      zeta_ref = azeta_ref;
      weight = aweight;
      L = lam.size();
      pseudo_type = apseudo_type; 

      set_num_residuals(2*L); // first L real parts then L imaginary parts 
      mutable_parameter_block_sizes()->push_back(N-1); // Real parts of (L1_11,...)
      mutable_parameter_block_sizes()->push_back(N-1); // Imaginary parts of (L1_11,...)
      
    }
    
    DtnCostFunction_reduced(int aN, RealVector alam,ComplexVector azeta_ref,RealVector aweight, std::string apseudo_type,double* a_smallest_sigma)  {
      N = aN;
      lam = alam;
      zeta_ref = azeta_ref;
      weight = aweight;
      L = lam.size();
      pseudo_type = apseudo_type; 
      smallest_sigma = a_smallest_sigma;  

      set_num_residuals(2*L); // first L real parts then L imaginary parts 
      mutable_parameter_block_sizes()->push_back(N-1); // Real parts of (L1_11,...)
      mutable_parameter_block_sizes()->push_back(N-1); // Imaginary parts of (L1_11,...)
      
    }

    DtnCostFunction_reduced(int aN, RealVector alam,ComplexVector azeta_ref,RealVector aweight, std::string apseudo_type, double asing_tol) {
      N = aN;
      lam = alam;
      zeta_ref = azeta_ref;
      weight = aweight;
      L = lam.size();
      pseudo_type = apseudo_type; 
      sing_tol = asing_tol; 

      set_num_residuals(2*L); // first L real parts then L imaginary parts 
      mutable_parameter_block_sizes()->push_back(N-1); // Real parts of (L1_11,...)
      mutable_parameter_block_sizes()->push_back(N-1); // Imaginary parts of (L1_11,...)

    }
    
    DtnCostFunction_reduced(int aN, RealVector alam,ComplexVector azeta_ref,RealVector aweight, std::string apseudo_type, double asing_tol, 
		            double* a_smallest_sigma ) {
      N = aN;
      lam = alam;
      zeta_ref = azeta_ref;
      weight = aweight;
      L = lam.size();
      pseudo_type = apseudo_type; 
      sing_tol = asing_tol; 
      smallest_sigma = a_smallest_sigma;  

      set_num_residuals(2*L); // first L real parts then L imaginary parts 
      mutable_parameter_block_sizes()->push_back(N-1); // Real parts of (L1_11,...)
      mutable_parameter_block_sizes()->push_back(N-1); // Imaginary parts of (L1_11,...)

    }

    void SetSingTol(double new_tol) {
      sing_tol = new_tol;
    }

    virtual ~DtnCostFunction_reduced() {}
    virtual bool Evaluate(double const* const* parameters,
		          double* residuals,
			  double** jacobians) const {
    
     ComplexMatrix A(L,N+1);
     
     
     for (int l = 0; l < L; l++) {
       A(l,0) = weight(l);
       A(l,1) = weight(l)*lam(l);
       for (int j= 0; j < N-1; j++) {
         A(l,2+j) = -weight(l)/( Complex(parameters[0][j],parameters[1][j])+lam(l) );
       } 
     }

    if (smallest_sigma != NULL) {  
      auto A_sigma = A.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV);
      *smallest_sigma = A_sigma.singularValues()(A_sigma.singularValues().size()-1); 
    } 
       
     ComplexVector b = zeta_ref;
     for(int l = 0; l < b.size(); l++) {
       b(l) *= weight(l);
     }

     
     if(pseudo_type == "SVD" || pseudo_type == "direct" || pseudo_type == "COD_pseudo") {
       
       ComplexMatrix A_dagger;
       ComplexMatrix AH_dagger;
       
       if(pseudo_type == "direct") {
         ComplexMatrix AH_A = A.adjoint() * A;
         ComplexMatrix AH_A_inv = AH_A.inverse();
         A_dagger = AH_A_inv * A.adjoint();
	 AH_dagger = A_dagger.adjoint();
       }
       
       if(pseudo_type == "SVD") {
         auto A_svd = A.jacobiSvd(Eigen::ComputeFullU | Eigen::ComputeFullV);
         std::cout << "cond(A) = " << A_svd.singularValues()(0) / 
	                          A_svd.singularValues()( A_svd.singularValues().size()-1) 
			       << std::endl;      			       
         std::cout << "sigma(A) =  " << A_svd.singularValues() << std::endl;
         auto U_A = A_svd.matrixU();
         auto V_A = A_svd.matrixV();
         RealMatrix S_A(L,N+1);
         S_A.setZero();
         for(int i=0; i < A_svd.singularValues().size(); i++) {
           if (A_svd.singularValues()(i) > sing_tol) {
           S_A(i,i) = 1/A_svd.singularValues()(i);
           }
           else {
             std::cout << "A_svd.singularValues()( " << i << ") = " << A_svd.singularValues()(i) << std::endl;
           }
         }
         auto S_dagger = S_A.transpose();
         A_dagger = V_A * S_dagger * U_A.adjoint();
         AH_dagger = A_dagger.adjoint();
       }
      
       if(pseudo_type == "COD_pseudo") {    
         auto A_QR = A.completeOrthogonalDecomposition();
         A_dagger = A_QR.pseudoInverse();
         AH_dagger = A_dagger.adjoint();
       }

       ComplexVector fz = A * A_dagger * b -b;
       
       for( int l = 0; l < L; l++) {
         residuals[l] = fz(l).real();
         residuals[l+L] = fz(l).imag();
       }
     
       if (jacobians != NULL) {
     
         ComplexMatrix dA_real(L,N+1);
         ComplexMatrix dA_imag(L,N+1);
    
         ComplexVector Adagger_b = A_dagger * b;  
         ComplexVector tmpX = b - A * Adagger_b;
       
         for(int j = 0; j < N-1; j++) { 
           dA_real.setZero();
	 
	   for(int l = 0; l < L; l++) {
	     dA_real(l,2+j) = weight(l)/ std::pow((Complex(parameters[0][j],parameters[1][j])+lam(l)),2);
	   }

           ComplexVector tmp1_dreal  = dA_real * Adagger_b; 
	   ComplexVector tmp1_dimag  = Complex(0,1)*tmp1_dreal;
	   ComplexVector tmpA = dA_real * Adagger_b; 
	   ComplexVector tmp2_dreal  = - A * A_dagger * tmpA ;   
           ComplexVector tmp2_dimag  = - A * A_dagger * (Complex(0,1) * tmpA );  
	   ComplexVector tmp3_dreal  = A * A_dagger * ( AH_dagger * ( dA_real.adjoint()*tmpX ) );
	   ComplexVector tmp3_dimag  = A * A_dagger * ( AH_dagger *  ( Complex(0,-1) * dA_real.adjoint()*tmpX ) );	
	   ComplexVector f_dreal = tmp1_dreal + tmp2_dreal + tmp3_dreal;
           ComplexVector f_dimag = tmp1_dimag + tmp2_dimag + tmp3_dimag;
	 
	   for(int l = 0; l < L; l++) {
             if (jacobians[0] != NULL) {
               jacobians[0][l*(N-1)+j] = f_dreal(l).real();
               jacobians[0][l*(N-1)+j+ L*(N-1)] = f_dreal(l).imag();
	     }
             if (jacobians[1] != NULL) {
               jacobians[1][l*(N-1)+j] = f_dimag(l).real();
               jacobians[1][l*(N-1)+j+ L*(N-1)] = f_dimag(l).imag();
	     }  
       	   }
         }
       }

     }
     else {

       if(pseudo_type == "colPivH") {
         
         auto A_QR = A.colPivHouseholderQr();
         auto AH_QR = A.adjoint().colPivHouseholderQr();
	 
	 ComplexVector fz = A * A_QR.solve(b) - b;
         for( int l = 0; l < L; l++) {
           residuals[l] = fz(l).real();
           residuals[l+L] = fz(l).imag();
         }
       
         if (jacobians != NULL) {
     
           ComplexMatrix dA_real(L,N+1);
           ComplexMatrix dA_imag(L,N+1);
       
           ComplexVector Adagger_b = A_QR.solve(b);  
           ComplexVector tmpX = b - A * Adagger_b;
       
           for(int j = 0; j < N-1; j++) { 
             dA_real.setZero();
	 
	     for(int l = 0; l < L; l++) {
	       dA_real(l,2+j) = weight(l)/ std::pow((Complex(parameters[0][j],parameters[1][j])+lam(l)),2);
	     }

             ComplexVector tmp1_dreal  = dA_real * Adagger_b; 
	     ComplexVector tmp1_dimag  = Complex(0,1)*tmp1_dreal;
	     ComplexVector tmpA = dA_real * Adagger_b; 
	     ComplexVector tmp2_dreal  = - A * A_QR.solve( tmpA );   
             ComplexVector tmp2_dimag  = - A * A_QR.solve( Complex(0,1) * tmpA );  
	     ComplexVector tmp3_dreal  = A * A_QR.solve( AH_QR.solve( dA_real.adjoint()*tmpX ) );
	     ComplexVector tmp3_dimag  = A * A_QR.solve( AH_QR.solve( Complex(0,-1) * dA_real.adjoint()*tmpX ) );	
	 
	     ComplexVector f_dreal = tmp1_dreal + tmp2_dreal + tmp3_dreal;
             ComplexVector f_dimag = tmp1_dimag + tmp2_dimag + tmp3_dimag;
	 
	     // ComplexVector tmp1_dimag  = dA_imag * Adagger_b;
             // ComplexVector tmp2_dimag  = - A * A_QR.solve( dA_imag * Adagger_b );  
             // ComplexVector tmpX_dimag  = b - A * Adagger_b;
	     // ComplexVector tmp3_dimag  =  AH_QR.solve( dA_imag.adjoint()*tmpX_dimag );
	     // ComplexVector tmp3_dimag  = A * A_QR.solve( AH_QR.solve( dA_imag.adjoint()*tmpX_dimag ) );
             // ComplexVector f_dimag = tmp1_dimag + tmp2_dimag + tmp3_dimag;
	 
	     for(int l = 0; l < L; l++) {
               if (jacobians[0] != NULL) {
                 jacobians[0][l*(N-1)+j] = f_dreal(l).real();
                 jacobians[0][l*(N-1)+j+ L*(N-1)] = f_dreal(l).imag();
	       }
               if (jacobians[1] != NULL) {
                 jacobians[1][l*(N-1)+j] = f_dimag(l).real();
                 jacobians[1][l*(N-1)+j+ L*(N-1)] = f_dimag(l).imag();
	       }
       	     }
           }	 
         }
       
       }

       if(pseudo_type == "COD_solve") {
         
         auto A_QR = A.completeOrthogonalDecomposition();
         auto AH_QR = A.adjoint().completeOrthogonalDecomposition();
	 
	 ComplexVector fz = A * A_QR.solve(b) - b;
         for( int l = 0; l < L; l++) {
           residuals[l] = fz(l).real();
           residuals[l+L] = fz(l).imag();
         }
       
         if (jacobians != NULL) {
     
           ComplexMatrix dA_real(L,N+1);
           ComplexMatrix dA_imag(L,N+1);
       
           ComplexVector Adagger_b = A_QR.solve(b);  
           ComplexVector tmpX = b - A * Adagger_b;
       
           for(int j = 0; j < N-1; j++) { 
             dA_real.setZero();
	 
	     for(int l = 0; l < L; l++) {
	       dA_real(l,2+j) = weight(l)/ std::pow((Complex(parameters[0][j],parameters[1][j])+lam(l)),2);
	     }

             ComplexVector tmp1_dreal  = dA_real * Adagger_b; 
	     ComplexVector tmp1_dimag  = Complex(0,1)*tmp1_dreal;
	     ComplexVector tmpA = dA_real * Adagger_b; 
	     ComplexVector tmp2_dreal  = - A * A_QR.solve( tmpA );   
             ComplexVector tmp2_dimag  = - A * A_QR.solve( Complex(0,1) * tmpA );  
	     
	     // ComplexVector tmp3_dreal  = A * A_QR.solve( AH_QR.solve( dA_real.adjoint()*tmpX ) );
	     // ComplexVector tmp3_dimag  = A * A_QR.solve( AH_QR.solve( Complex(0,-1) * dA_real.adjoint()*tmpX ) );	
	     ComplexVector tmp3_dreal  = AH_QR.solve( dA_real.adjoint()*tmpX ) ;
	     ComplexVector tmp3_dimag  = AH_QR.solve( Complex(0,-1) * dA_real.adjoint()*tmpX ) ;	
	 
	     ComplexVector f_dreal = tmp1_dreal + tmp2_dreal + tmp3_dreal;
             ComplexVector f_dimag = tmp1_dimag + tmp2_dimag + tmp3_dimag;
	 
	     // ComplexVector tmp1_dimag  = dA_imag * Adagger_b;
             // ComplexVector tmp2_dimag  = - A * A_QR.solve( dA_imag * Adagger_b );  
             // ComplexVector tmpX_dimag  = b - A * Adagger_b;
	     // ComplexVector tmp3_dimag  =  AH_QR.solve( dA_imag.adjoint()*tmpX_dimag );
	     // ComplexVector tmp3_dimag  = A * A_QR.solve( AH_QR.solve( dA_imag.adjoint()*tmpX_dimag ) );
             // ComplexVector f_dimag = tmp1_dimag + tmp2_dimag + tmp3_dimag;
	 
	     for(int l = 0; l < L; l++) {
               if (jacobians[0] != NULL) {
                 jacobians[0][l*(N-1)+j] = f_dreal(l).real();
                 jacobians[0][l*(N-1)+j+ L*(N-1)] = f_dreal(l).imag();
	       }
               if (jacobians[1] != NULL) {
                 jacobians[1][l*(N-1)+j] = f_dimag(l).real();
                 jacobians[1][l*(N-1)+j+ L*(N-1)] = f_dimag(l).imag();
	       }
       	     }
           }	 
         }
       
       }
       
       if(pseudo_type == "COD_weighted") {
        
	 // Compute QR decompositions of A and AH
	 Eigen::ColPivHouseholderQR<ComplexMatrix> AH_QR(A.adjoint().rows(),A.adjoint().cols());
	 AH_QR.compute(A.adjoint()); 
	 Eigen::ColPivHouseholderQR<ComplexMatrix> A_QR(A.rows(),A.cols());
	 A_QR.compute(A);
	  
	 // Compute QR decompositions of adjoint of R-matrices
	 // std::cout << ComplexMatrix( AH_QR.matrixR().triangularView<Eigen::Upper>().adjoint() ) << std::endl;
	 // ComplexMatrix RH_AH = AH_QR.matrixR().topLeftCorner(AH_QR.rank(),AH_QR.rank()).triangularView<Eigen::Upper>().adjoint();
	 ComplexMatrix RH_AH = AH_QR.matrixR().triangularView<Eigen::Upper>().adjoint();
	 // std::cout << "RH_AH = " << RH_AH << std::endl; 
	 Eigen::HouseholderQR<ComplexMatrix> RH_AH_QR(RH_AH.rows(),RH_AH.cols());
	 RH_AH_QR.compute(RH_AH);
	 // ComplexMatrix RH_A = A_QR.matrixR().topLeftCorner(A_QR.rank(),A_QR.rank()).triangularView<Eigen::Upper>().adjoint();
	 ComplexMatrix RH_A = A_QR.matrixR().triangularView<Eigen::Upper>().adjoint();
	 Eigen::HouseholderQR<ComplexMatrix> RH_A_QR(RH_A.rows(),RH_A.cols());
         RH_A_QR.compute(RH_A);

	 
         // auto AH_COD = A.adjoint().completeOrthogonalDecomposition();
	 
	 // ComplexVector fz = A * A_QR.solve(b) - b;
	 ComplexVector fz = A * Solve_weighted(b,AH_QR,RH_AH_QR)- b;
         for( int l = 0; l < L; l++) {
           residuals[l] = fz(l).real();
           residuals[l+L] = fz(l).imag();
         }
       
         if (jacobians != NULL) {
     
           ComplexMatrix dA_real(L,N+1);
           ComplexMatrix dA_imag(L,N+1);
       
           // ComplexVector Adagger_b = A_QR.solve(b);  
           ComplexVector Adagger_b = Solve_weighted(b,AH_QR,RH_AH_QR);  
           ComplexVector tmpX = b - A * Adagger_b;
       
           for(int j = 0; j < N-1; j++) { 
             dA_real.setZero();
	 
	     for(int l = 0; l < L; l++) {
	       dA_real(l,2+j) = weight(l)/ std::pow((Complex(parameters[0][j],parameters[1][j])+lam(l)),2);
	     }

             ComplexVector tmp1_dreal  = dA_real * Adagger_b; 
	     ComplexVector tmp1_dimag  = Complex(0,1)*tmp1_dreal;
	     ComplexVector tmpA = dA_real * Adagger_b; 
	     
	     // ComplexVector tmp2_dreal  = - A * A_QR.solve( tmpA );   
             // ComplexVector tmp2_dimag  = - A * A_QR.solve( Complex(0,1) * tmpA );  
	     ComplexVector tmp2_dreal  = - A * Solve_weighted(tmpA,AH_QR,RH_AH_QR);   
             ComplexVector tmp2_dimag  = - A * Solve_weighted( ComplexVector(Complex(0,1) * tmpA),AH_QR,RH_AH_QR);  
	     
	     //ComplexVector tmp3_dreal  = A * A_QR.solve( AH_QR.solve( dA_real.adjoint()*tmpX ) );
	     //ComplexVector tmp3_dimag  = A * A_QR.solve( AH_QR.solve( Complex(0,-1) * dA_real.adjoint()*tmpX ) );	
	     
	     // ComplexVector tmp3_dreal  = A * Solve_weighted(ComplexVector(Solve_weighted( ComplexVector(dA_real.adjoint()*tmpX),A_QR,RH_A_QR)),AH_QR,RH_AH_QR);
	     // ComplexVector tmp3_dimag  = A * Solve_weighted(ComplexVector(Solve_weighted(ComplexVector( Complex(0,-1) * dA_real.adjoint()*tmpX),A_QR,RH_A_QR)),AH_QR,RH_AH_QR);	 
	     ComplexVector tmp3_dreal  = ComplexVector(Solve_weighted( ComplexVector(dA_real.adjoint()*tmpX),A_QR,RH_A_QR));
	     ComplexVector tmp3_dimag  = ComplexVector(Solve_weighted(ComplexVector( Complex(0,-1) * dA_real.adjoint()*tmpX),A_QR,RH_A_QR));	
	 
	     ComplexVector f_dreal = tmp1_dreal + tmp2_dreal + tmp3_dreal;
             ComplexVector f_dimag = tmp1_dimag + tmp2_dimag + tmp3_dimag;
	 
	 
	     for(int l = 0; l < L; l++) {
               if (jacobians[0] != NULL) {
                 jacobians[0][l*(N-1)+j] = f_dreal(l).real();
                 jacobians[0][l*(N-1)+j+ L*(N-1)] = f_dreal(l).imag();
	       }
               if (jacobians[1] != NULL) {
                 jacobians[1][l*(N-1)+j] = f_dimag(l).real();
                 jacobians[1][l*(N-1)+j+ L*(N-1)] = f_dimag(l).imag();
	       }
       	     }
           }	 
         }
       }

     }
     

    
     // std::cout << "A_dagger_direct = " << A_dagger_direct << std::endl;
     /*
     std::cout << "A_dagger_direct.cols() = " <<  A_dagger_direct.cols() << ", A_dagger_direct.rows() = " << A_dagger_direct.rows() << std::endl;
     
     for (int i = 0; i < A_dagger_direct.rows(); i++) {
       for( int j = 0; j < A_dagger_direct.cols(); j++) {
         std::cout << "A_dagger_direct[" << i << "," << j << "] = " << A_dagger_direct(i,j) << std::endl; 
       }
     }
     
     std::cout << std::endl;
     auto A_QR = A.completeOrthogonalDecomposition();
     Eigen::MatrixXd P_A = A_QR.colsPermutation();
     std::cout << P_A << std::endl; 
     
     auto A_dagger = P_A*A_QR.pseudoInverse();
     
     std::cout << "A_dagger = " << A_dagger << std::endl;
     
     std::cout << "A_dagger.cols() = " <<  A_dagger.cols() << ", A_dagger.rows() = " << A_dagger.rows() << std::endl;
      
     for (int i = 0; i < A_dagger.rows(); i++) {
       for( int j = 0; j < A_dagger.cols(); j++) {
         std::cout << "A_dagger[" << i << "," << j << "] = " << A_dagger(i,j) << std::endl; 
       }
     } */ 




     // auto A_QR = A.colPivHouseholderQr();
     
     // auto AH_QR = A.adjoint().colPivHouseholderQr();
     
     //auto AH_QR = A.adjoint().completeOrthogonalDecomposition();
     
     // auto AH_dagger = AH_QR.pseudoInverse();
     // auto AH_dagger = A_dagger.adjoint();
    
       
     // auto A_QR = A.fullPivHouseholderQr();
     // auto AH_QR = A.adjoint().fullPivHouseholderQr();

     // auto A_QR = A.householderQr();
     // auto AH_QR = A.adjoint().householderQr();
     // std::cout << "A_dagger = " << A_dagger << std::endl;  
     //ComplexVector fz = A * A_dagger * b - b;
    

      
      return true;
    }
};




class DtnCostFunction_full_interval : public ceres::CostFunction {
  public:
    int N;
    double lam; 
    Complex dtn_ref;
    Complex g_omega;  

    DtnCostFunction_full_interval(int aN,double alam,Complex ag_omega,Complex adtn_ref) {
      N = aN;
      lam = alam;
      dtn_ref = adtn_ref;
      g_omega = ag_omega; 
      set_num_residuals(2);
      for(int i = 0; i < 6; i++){
        mutable_parameter_block_sizes()->push_back(N*N);
      }
    }
    virtual ~DtnCostFunction_full_interval() {}
    virtual bool Evaluate(double const* const* parameters,
		          double* residuals,
			  double** jacobians) const {
      
      
      ComplexMatrix S(N,N); 
      ComplexMatrix SE;
      ComplexMatrix SE_inv;
      
      if(N > 1) { 
        SE.resize(N-1,N-1);
      }
       
      Complex diff = Complex(0.0,0.0);
     
      for (int i = 0; i < N; i++){
        for(int j=  0; j < N; j++){
          S(i,j) = Complex(parameters[0][j+i*N],parameters[1][j+i*N]) 
		   + g_omega*Complex(parameters[4][j+i*N],parameters[5][j+i*N]) 
		   + lam*Complex(parameters[2][j+i*N],parameters[3][j+i*N]);
	   
	  if (N > 1 && i > 0 && j > 0) {
	    SE(i-1,j-1) = S(i,j);
	  }
	}
      }

      if( N > 1) {
        SE_inv = SE.inverse();
      }
      Complex dtn_approx = S(0,0);
      if (N > 1) {
        for (int i=1;i<N;i++){
          for(int j=1;j<N;j++){
            dtn_approx -= S(0,i)*SE_inv(i-1,j-1)*S(j,0);
	  }
	}
      }
      diff = (dtn_ref-dtn_approx);

      residuals[0] = diff.real();
      residuals[1] = diff.imag();

      if (jacobians != NULL) { 
          	 	
        ComplexMatrix D_SE_inv;
        for (int k = 0; k < N; k++) {
          for(int m = 0; m < N; m++) {
            Complex d_dtn_app_km = Complex(0,0);

            if ( k > 0 && m > 0) { 
              D_SE_inv.resize(N-1,N-1);
	      for(int i = 0; i < N-1; i++) {
	        for(int j = 0; j < N-1; j++) {
	          D_SE_inv(i,j) = -SE_inv(i,k-1)*SE_inv(m-1,j);
	        }
	      }
	    }


	    if (k==0 && m ==0) {
              d_dtn_app_km = -Complex(1,0);
	    }
            if (k >= 1 && m == 0) {
	      for(int i=1; i < N; i++) {
	        d_dtn_app_km += S(0,i)*SE_inv(i-1,k-1);
	      }
	    } 
            if (k == 0 && m >= 1) {
	      for(int j=1; j < N; j++) {
	        d_dtn_app_km += S(j,0)*SE_inv(m-1,j-1);
	      }
	    }
	    if ( k >=1 && m >= 1) { 
	      for(int i = 1; i < N; i++) {
	        for(int j = 1; j < N; j++) {
	          d_dtn_app_km += S(0,i)*D_SE_inv(i-1,j-1)*S(j,0);
	        }
	      }
	    }

            if (jacobians[0] != NULL) {
              jacobians[0][m+k*N] = d_dtn_app_km.real();
              jacobians[0][m+k*N+N*N] = d_dtn_app_km.imag();
            }
            if (jacobians[1] != NULL) {
              jacobians[1][m+k*N] = (Complex(0,1)*d_dtn_app_km).real();
              jacobians[1][m+k*N+N*N] = (Complex(0,1)*d_dtn_app_km).imag();
            }
            if (jacobians[2] != NULL) {
              jacobians[2][m+k*N] = (lam*d_dtn_app_km).real();
              jacobians[2][m+k*N+N*N] = (lam*d_dtn_app_km).imag();
            }
            if (jacobians[3] != NULL) {
              jacobians[3][m+k*N] = (Complex(0,1)*lam*d_dtn_app_km).real();
              jacobians[3][m+k*N+N*N] = (Complex(0,1)*lam*d_dtn_app_km).imag();
            }
            if (jacobians[4] != NULL) {
              jacobians[4][m+k*N] =  (g_omega*d_dtn_app_km).real();
              jacobians[4][m+k*N+N*N] = (g_omega*d_dtn_app_km).imag();
            }
            if (jacobians[5] != NULL) {
              jacobians[5][m+k*N] =  (Complex(0,1)*g_omega*d_dtn_app_km).real();
              jacobians[5][m+k*N+N*N] = (Complex(0,1)*g_omega*d_dtn_app_km).imag();
            }
	  }
        } 
      }
      
      return true;
    }
};



class DtnCostFunction_sym_interval : public ceres::CostFunction {
  public:
    int N;
    int N_mat; 
    double lam; 
    Complex dtn_ref;
    Complex g_omega;  

    DtnCostFunction_sym_interval(int aN,double alam,Complex ag_omega,Complex adtn_ref) {
      N = aN;
      N_mat = N*(N+1)/2;
      //std::cout << "N_mat = " << N_mat << std::endl; 
      lam = alam;
      dtn_ref = adtn_ref;
      g_omega = ag_omega; 
      set_num_residuals(2);
      for(int i = 0; i < 6; i++){
        mutable_parameter_block_sizes()->push_back(N_mat);
      }
    }
    virtual ~DtnCostFunction_sym_interval() {}
    virtual bool Evaluate(double const* const* parameters,
		          double* residuals,
			  double** jacobians) const {
      
      
      ComplexMatrix S(N,N); 
      ComplexMatrix SE;
      ComplexMatrix SE_inv;
      
      if(N > 1) { 
        SE.resize(N-1,N-1);
      }
       
      Complex diff = Complex(0.0,0.0);
     
      for (int i = 0; i < N; i++){
        for(int j = i; j < N; j++){
	  int idx_lin  = (N*(N-1)/2) - int((N-i)*(N-i-1)/2) + j;
          S(i,j) = Complex(parameters[0][idx_lin],parameters[1][idx_lin]) 
		   + g_omega*Complex(parameters[4][idx_lin],parameters[5][idx_lin]) 
		   + lam*Complex(parameters[2][idx_lin],parameters[3][idx_lin]);
	   
	  if (N > 1 && i > 0 && j > 0) {
	    SE(i-1,j-1) = S(i,j);
	  }
	}
      }

      for (int i = 0; i < N; i++){
        for(int j=  0; j < i; j++){
          S(i,j) = S(j,i);
	  if (N > 1 && i > 0 && j > 0) {
	    SE(i-1,j-1) = S(i,j);
	  }
	}
      }
      // std::cout << "a" << std::endl; 

      if( N > 1) {
        SE_inv = SE.inverse();
      }
      Complex dtn_approx = S(0,0);
      if (N > 1) {
        for (int i=1;i<N;i++){
          for(int j=1;j<N;j++){
            dtn_approx -= S(0,i)*SE_inv(i-1,j-1)*S(j,0);
	  }
	}
      }
      diff = (dtn_ref-dtn_approx);

      residuals[0] = diff.real();
      residuals[1] = diff.imag();

      if (jacobians != NULL) { 
          	 	
      // std::cout << "b" << std::endl; 

        ComplexMatrix D_SE_inv;
        for (int k = 0; k < N; k++) {
          for(int m = k; m < N; m++) {
	    
	    int idx_lin  = (N*(N-1)/2) - int((N-k)*(N-k-1)/2) + m;
            Complex d_dtn_app_km = Complex(0,0);

            if ( k > 0 && m > 0) { 
              D_SE_inv.resize(N-1,N-1);
	      for(int i = 0; i < N-1; i++) {
	        for(int j = 0; j < N-1; j++) {
	          if ( k == m) {
	            D_SE_inv(i,j) = -SE_inv(i,k-1)*SE_inv(m-1,j);
	          }
		  else {
	            D_SE_inv(i,j) = -SE_inv(i,k-1)*SE_inv(m-1,j) -SE_inv(i,m-1)*SE_inv(k-1,j);
		  }
		}
	      }
	    }

	    if (k==0 && m ==0) {
              d_dtn_app_km = -Complex(1,0);
	    }
            if (k == 0 && m > 0) {
	      for(int i=1; i < N; i++) {
	        d_dtn_app_km += S(0,i)*SE_inv(i-1,m-1) +  S(i,0)*SE_inv(m-1,i-1);
	      }
	    } 
	    if ( k >=1 && m >= 1) { 
	      for(int i = 1; i < N; i++) {
	        for(int j = 1; j < N; j++) {
	          d_dtn_app_km += S(0,i)*D_SE_inv(i-1,j-1)*S(j,0);
	        }
	      }
	    }

            if (jacobians[0] != NULL) {
              jacobians[0][idx_lin] = d_dtn_app_km.real();
              jacobians[0][idx_lin+N_mat] = d_dtn_app_km.imag();
            }
            if (jacobians[1] != NULL) {
              jacobians[1][idx_lin] = (Complex(0,1)*d_dtn_app_km).real();
              jacobians[1][idx_lin+N_mat] = (Complex(0,1)*d_dtn_app_km).imag();
            }
            if (jacobians[2] != NULL) {
              jacobians[2][idx_lin] = (lam*d_dtn_app_km).real();
              jacobians[2][idx_lin+N_mat] = (lam*d_dtn_app_km).imag();
            }
            if (jacobians[3] != NULL) {
              jacobians[3][idx_lin] = (Complex(0,1)*lam*d_dtn_app_km).real();
              jacobians[3][idx_lin+N_mat] = (Complex(0,1)*lam*d_dtn_app_km).imag();
            }
            if (jacobians[4] != NULL) {
              jacobians[4][idx_lin] =  (g_omega*d_dtn_app_km).real();
              jacobians[4][idx_lin+N_mat] = (g_omega*d_dtn_app_km).imag();
            }
            if (jacobians[5] != NULL) {
              jacobians[5][idx_lin] =  (Complex(0,1)*g_omega*d_dtn_app_km).real();
              jacobians[5][idx_lin+N_mat] = (Complex(0,1)*g_omega*d_dtn_app_km).imag();
            }
	  }
        } 
      }
      
      return true;
    }
};

double solve_problem(double x0) {

  double x = x0;
  const double initial_x = x;
  Problem problem; 
   CostFunction* cost_function =
      new AutoDiffCostFunction<CostFunctor, 1, 1>(new CostFunctor);
  // CostFunction* cost_function =
  //     new DtnCostFunction; 
  problem.AddResidualBlock(cost_function, NULL, &x); 
  Solver::Options options;
  options.minimizer_progress_to_stdout = true;
  options.check_gradients = true;
  Solver::Summary summary;
  Solve(options, &problem, &summary);

  std::cout << summary.BriefReport() << "\n";
  std::cout << "x : " << initial_x
            << " -> " << x << "\n";

  return x;
}


class learned_dtn
{
  protected:

  public:
    RealVector lam; // eigenvalues
    ComplexVector zeta_ref; // dtn numbers 
    RealVector weight; // weights
    
    learned_dtn(RealVector alam,ComplexVector azeta_ref,RealVector aweight)  { 
      lam = alam; 
      zeta_ref = azeta_ref;     
      weight = aweight;    
    }
      
    void Run_full( Eigen::Ref<ComplexMatrix> Lone_guess,
	           Eigen::Ref<ComplexMatrix> Ltwo_guess,
		   pybind11::dict flags,
		   Eigen::Ref<RealVector> final_res
                 ) { 

	int N = Lone_guess.cols();
	
	RealVector L1_real(N*N);
	RealVector L1_imag(N*N);
	RealVector L2_real(N*N);
	RealVector L2_imag(N*N);

        for (int i = 0; i < N; i++) {
          for(int j= 0; j < N; j++) {
	    L1_real(j+i*N) = Lone_guess.real()(i,j);
	    L1_imag(j+i*N) = Lone_guess.imag()(i,j);
	    L2_real(j+i*N) = Ltwo_guess.real()(i,j);
	    L2_imag(j+i*N) = Ltwo_guess.imag()(i,j);
	  }
	}


	Problem problem; 
        for(int i = 0; i < lam.size(); i++) {	
	  CostFunction* cost_function =
	    new DtnCostFunction_full(N,lam[i],zeta_ref[i]); 
	  problem.AddResidualBlock(cost_function, new ScaledLoss(nullptr,weight[i],TAKE_OWNERSHIP), L1_real.data(),L1_imag.data(),L2_real.data(),L2_imag.data()); 
	}
	Solver::Options options;
	SetSolverOptions(options,flags);
	Solver::Summary summary;
	Solve(options, &problem, &summary);

	if (flags.contains("report_level")) {
	  if (std::string(pybind11::str(flags["report_level"])) == std::string("Brief")){
	    std::cout << summary.BriefReport() << "\n";
	  }
	  if (std::string(pybind11::str(flags["report_level"])) == std::string("Full")){
	    std::cout << summary.FullReport() << "\n";
	  }
	  if (std::string(pybind11::str(flags["report_level"])) == std::string("solver_stats")){
	    if (flags.contains("solver_stats")) {
              std::cout << "iterations: " << summary.num_successful_steps + summary.num_unsuccessful_steps
		        << ", initial cost: " << summary.initial_cost 
			<< ", final cost: " << summary.final_cost 
			<< ", total time [s]: " << summary.total_time_in_seconds 
			<< std::endl;
	      flags["solver_stats"]["initial_cost"] = summary.initial_cost;
	      flags["solver_stats"]["final_cost"] = summary.final_cost;
	      flags["solver_stats"]["iterations"] = summary.num_successful_steps + summary.num_unsuccessful_steps;
	      flags["solver_stats"]["total_time_in_seconds"] = summary.total_time_in_seconds;
	    }
	  }
	}

	if( final_res.size() == lam.size()) {
          auto evopt = Problem::EvaluateOptions();
          evopt.apply_loss_function = false;
	  std::vector<double> afinal_res;
	  problem.Evaluate(evopt,nullptr,&afinal_res,nullptr,nullptr);
	  int k = 0;
	  int kk = 0;
	  while(kk < afinal_res.size()-1) {
	    final_res(k) = sqrt(afinal_res[kk]*afinal_res[kk] + afinal_res[kk+1]*afinal_res[kk+1])/abs(zeta_ref(k));
	    k += 1;
	    kk += 2;
	  }
	}
        
	for (int i = 0; i < N; i++) {
          for(int j= 0; j < N; j++) {
	    Lone_guess(i,j) = Complex(L1_real(j+i*N),L1_imag(j+i*N));
	    Ltwo_guess(i,j) = Complex(L2_real(j+i*N),L2_imag(j+i*N));
	  }
	}
    }

    void Run_medium( Eigen::Ref<ComplexMatrix> Lone_guess,
	             Eigen::Ref<ComplexMatrix> Ltwo_guess,
		     pybind11::dict flags,
		     Eigen::Ref<RealVector> final_res
                   ) { 

	int N = Lone_guess.cols();
	
	// ordering of vectors:
	// S_II = [Re(Lone),Im(Lone),Re(Ltwo),Im(Ltwo)] 
	
	RealVector S_II(4);
        
	S_II(0) = Lone_guess.real()(0,0);
        S_II(1) = Lone_guess.imag()(0,0);
        S_II(2) = Ltwo_guess.real()(0,0);
        S_II(3) = Ltwo_guess.imag()(0,0);

	RealVector S_IE;
	RealVector S_EI;
	RealVector S_EE;

        if ( N > 1) {
	  
	  S_IE.resize(4*(N-1));
	  S_EI.resize(4*(N-1));
	  S_EE.resize(4*(N-1));
	  
	  for(int i=1; i <N; i++) {

	    S_IE(i-1)           =  Lone_guess.real()(0,i);
	    S_IE(i-1 + N-1)     =  Lone_guess.imag()(0,i);
	    S_IE(i-1 + 2*(N-1)) =  Ltwo_guess.real()(0,i);
	    S_IE(i-1 + 3*(N-1)) =  Ltwo_guess.imag()(0,i);

	    S_EI(i-1)           =  Lone_guess.real()(i,0);
	    S_EI(i-1 + N-1)     =  Lone_guess.imag()(i,0);
	    S_EI(i-1 + 2*(N-1)) =  Ltwo_guess.real()(i,0);
	    S_EI(i-1 + 3*(N-1)) =  Ltwo_guess.imag()(i,0);

	    S_EE(i-1)           =  Lone_guess.real()(i,i);
	    S_EE(i-1 + N-1 )    =  Lone_guess.imag()(i,i);
	    S_EE(i-1 +2*(N-1))  =  Ltwo_guess.real()(i,i);
	    S_EE(i-1 +3*(N-1))  =  Ltwo_guess.imag()(i,i);
	  
	  }

	}	



	Problem problem; 
        for(int i = 0; i < lam.size(); i++) {	
	  CostFunction* cost_function =
	    new DtnCostFunction_medium(N,lam[i],zeta_ref[i]); 
	  if (N > 1) {
	    problem.AddResidualBlock(cost_function,new ScaledLoss(nullptr,weight[i],TAKE_OWNERSHIP), 
			             S_II.data(),S_IE.data(),S_EI.data(),S_EE.data() 
				    );
	  } 
	  else {
	    problem.AddResidualBlock(cost_function,new ScaledLoss(nullptr,weight[i],TAKE_OWNERSHIP),S_II.data());
	  }
	}
	Solver::Options options;
	SetSolverOptions(options,flags);
	Solver::Summary summary;
	Solve(options, &problem, &summary);
       
	if (flags.contains("report_level")) {
	  if (std::string(pybind11::str(flags["report_level"])) == std::string("Brief")){
	    std::cout << summary.BriefReport() << "\n";
	  }
	  if (std::string(pybind11::str(flags["report_level"])) == std::string("Full")){
	    std::cout << summary.FullReport() << "\n";
	  }
	}

	if( final_res.size() == lam.size()) {
          auto evopt = Problem::EvaluateOptions();
          evopt.apply_loss_function = false;
	  std::vector<double> afinal_res;
	  problem.Evaluate(evopt,nullptr,&afinal_res,nullptr,nullptr);
	  int k = 0;
	  int kk = 0;
	  while(kk < afinal_res.size()-1) {
	    final_res(k) = sqrt(afinal_res[kk]*afinal_res[kk] + afinal_res[kk+1]*afinal_res[kk+1])/abs(zeta_ref(k));
	    k += 1;
	    kk += 2;
	  }
	}

	Lone_guess.setZero();
        Ltwo_guess.setZero();

        Lone_guess.real()(0,0) = S_II(0);
        Lone_guess.imag()(0,0) = S_II(1);
        Ltwo_guess.real()(0,0) = S_II(2);
        Ltwo_guess.imag()(0,0) = S_II(3);

	if ( N > 1) {
         
          for(int i = 1; i < N; i++){
		
	    Lone_guess.real()(0,i) = S_IE(i-1);
	    Lone_guess.imag()(0,i) = S_IE(i-1 + N-1);
	    Ltwo_guess.real()(0,i) = S_IE(i-1 +2*(N-1)); 
	    Ltwo_guess.imag()(0,i) = S_IE(i-1 +3*(N-1)); 

	    Lone_guess.real()(i,0) = S_EI(i-1); 
	    Lone_guess.imag()(i,0) = S_EI(i-1 + N-1); 
	    Ltwo_guess.real()(i,0) = S_EI(i-1 +2*(N-1)); 
	    Ltwo_guess.imag()(i,0) = S_EI(i-1 +3*(N-1));

	    Lone_guess.real()(i,i) = S_EE(i-1);
	    Lone_guess.imag()(i,i) = S_EE(i-1 + (N-1));
	    Ltwo_guess.real()(i,i) = S_EE(i-1 +2*(N-1));
	    Ltwo_guess.imag()(i,i) = S_EE(i-1 +3*(N-1));

	  }
	}

    }

    void Run_minimal( Eigen::Ref<ComplexMatrix> Lone_guess,
	              Eigen::Ref<ComplexMatrix> Ltwo_guess,
		      pybind11::dict flags,
		      Eigen::Ref<RealVector> final_res
		    ) { 

	int N = Lone_guess.cols();
	
	// ordering of vectors:
	// S_II = [Re(Lone_II),Im(Lone_II),Re(Ltwo_II),Im(Ltwo_II)] 
	
	RealVector S_II(4);
        
	S_II(0) = Lone_guess.real()(0,0);
        S_II(1) = Lone_guess.imag()(0,0);
        S_II(2) = Ltwo_guess.real()(0,0);
        S_II(3) = Ltwo_guess.imag()(0,0);

	// S_IE = SE_I, S_IE_squared = S_IE*S_EI
	RealVector S_IE_squared; // 
	// S_EE = [Re(Lone_EE),Im(Lone_EE)]
	RealVector S_EE;

        if ( N > 1) {
	  
	  S_IE_squared.resize(2*(N-1));
	  S_EE.resize(2*(N-1));
	  
	  for(int i=1; i <N; i++) {
            Complex tmp = Lone_guess(0,i)*Lone_guess(i,0);
	    S_IE_squared(i-1)       =  tmp.real();
	    S_IE_squared(i-1 + N-1) =  tmp.imag();

	    S_EE(i-1)           =  Lone_guess.real()(i,i);
	    S_EE(i-1 + N-1 )    =  Lone_guess.imag()(i,i);
	  }
	}	

	Problem problem; 
        for(int i = 0; i < lam.size(); i++) {	
	  CostFunction* cost_function =
	    new DtnCostFunction_minimal(N,lam[i],zeta_ref[i]); 
	  if (N > 1) {
	    problem.AddResidualBlock(cost_function, new ScaledLoss(nullptr,weight[i],TAKE_OWNERSHIP), 
			             S_II.data(),S_IE_squared.data(),S_EE.data() 
				    );
	  } 
	  else {
	    problem.AddResidualBlock(cost_function, new ScaledLoss(nullptr,weight[i],TAKE_OWNERSHIP),S_II.data());
	  }
	}
	Solver::Options options;
	SetSolverOptions(options,flags);
	Solver::Summary summary;
	Solve(options, &problem, &summary);       

	if (flags.contains("report_level")) {
	  if (std::string(pybind11::str(flags["report_level"])) == std::string("Brief")){
	    std::cout << summary.BriefReport() << "\n";
	  }
	  if (std::string(pybind11::str(flags["report_level"])) == std::string("Full")){
	    std::cout << summary.FullReport() << "\n";
	  }
	}

	if( final_res.size() == lam.size()) {
          auto evopt = Problem::EvaluateOptions();
          evopt.apply_loss_function = false;
	  std::vector<double> afinal_res;
	  problem.Evaluate(evopt,nullptr,&afinal_res,nullptr,nullptr);
	  int k = 0;
	  int kk = 0;
	  while(kk < afinal_res.size()-1) {
	    final_res(k) = sqrt(afinal_res[kk]*afinal_res[kk] + afinal_res[kk+1]*afinal_res[kk+1])/abs(zeta_ref(k));
	    k += 1;
	    kk += 2;
	  }
	}

        Lone_guess.setZero();
        Ltwo_guess.setZero();
	
	Lone_guess.real()(0,0) = S_II(0);
        Lone_guess.imag()(0,0) = S_II(1);
        Ltwo_guess.real()(0,0) = S_II(2);
        Ltwo_guess.imag()(0,0) = S_II(3);

	if ( N > 1) {
         
          for(int i = 1; i < N; i++){
		
	    Lone_guess(0,i) = std::sqrt(Complex(S_IE_squared(i-1),S_IE_squared(i-1 + N-1))); 
	    Lone_guess(i,0) = std::sqrt(Complex(S_IE_squared(i-1),S_IE_squared(i-1 + N-1)));
	     
	    Lone_guess.real()(i,i) = S_EE(i-1);
	    Lone_guess.imag()(i,i) = S_EE(i-1 + (N-1));
	    Ltwo_guess(i,i) = Complex(1,0);

	  }
	}
    
    }

    void Run_minimalIC( Eigen::Ref<ComplexMatrix> Lone_guess,
	                Eigen::Ref<ComplexMatrix> Ltwo_guess,
			pybind11::dict flags,
		        Eigen::Ref<RealVector> final_res
		      ) { 

	int N = Lone_guess.cols();
	
	// ordering of vectors:
	// S_II = [Re(Lone),Im(Lone),Re(Ltwo),Im(Ltwo)] 
	
	RealVector S_II(4);
        
	S_II(0) = Lone_guess.real()(0,0);
        S_II(1) = Lone_guess.imag()(0,0);
        S_II(2) = Ltwo_guess.real()(0,0);
        S_II(3) = Ltwo_guess.imag()(0,0);

	RealVector S_IE;
	RealVector S_EI;
	RealVector S_EE;

        if ( N > 1) {
	  
	  S_IE.resize(4*(N-1));
	  S_EI.resize(2*(N-1));
	  S_EE.resize(2*(N-1));
	  
	  for(int i=1; i <N; i++) {

	    S_IE(i-1)           =  Lone_guess.real()(0,i);
	    S_IE(i-1 + N-1)     =  Lone_guess.imag()(0,i);
	    S_IE(i-1 + 2*(N-1)) =  Ltwo_guess.real()(0,i);
	    S_IE(i-1 + 3*(N-1)) =  Ltwo_guess.imag()(0,i);

	    S_EI(i-1)           =  Lone_guess.real()(i,0);
	    S_EI(i-1 + N-1)     =  Lone_guess.imag()(i,0);

	    S_EE(i-1)           =  Lone_guess.real()(i,i);
	    S_EE(i-1 + N-1 )    =  Lone_guess.imag()(i,i);
	  
	  }

	}	

	Problem problem; 
        for(int i = 0; i < lam.size(); i++) {	
	  CostFunction* cost_function =
	    new DtnCostFunction_minimalIC(N,lam[i],zeta_ref[i]); 
	  if (N > 1) {
	    problem.AddResidualBlock(cost_function,new ScaledLoss(nullptr,weight[i],TAKE_OWNERSHIP) , 
			             S_II.data(),S_IE.data(),S_EI.data(),S_EE.data() 
				    );
	  } 
	  else {
	    problem.AddResidualBlock(cost_function,new ScaledLoss(nullptr,weight[i],TAKE_OWNERSHIP),S_II.data());
	  }
	}
	Solver::Options options;
	SetSolverOptions(options,flags);
	Solver::Summary summary;
	Solve(options, &problem, &summary);


	if (flags.contains("report_level")) {
	  if (std::string(pybind11::str(flags["report_level"])) == std::string("Brief")){
	    std::cout << summary.BriefReport() << "\n";
	  }
	  if (std::string(pybind11::str(flags["report_level"])) == std::string("Full")){
	    std::cout << summary.FullReport() << "\n";
	  }
	  if (std::string(pybind11::str(flags["report_level"])) == std::string("solver_stats")){
	    if (flags.contains("solver_stats")) {
              std::cout << "iterations: " << summary.num_successful_steps + summary.num_unsuccessful_steps
		        << ", initial cost: " << summary.initial_cost 
			<< ", final cost: " << summary.final_cost 
			<< ", total time [s]: " << summary.total_time_in_seconds 
			<< std::endl;
	      flags["solver_stats"]["initial_cost"] = summary.initial_cost;
	      flags["solver_stats"]["final_cost"] = summary.final_cost;
	      flags["solver_stats"]["iterations"] = summary.num_successful_steps + summary.num_unsuccessful_steps;
	      flags["solver_stats"]["total_time_in_seconds"] = summary.total_time_in_seconds;
	    }
	  }
	}

	if( final_res.size() == lam.size()) {
          auto evopt = Problem::EvaluateOptions();
          evopt.apply_loss_function = false;
	  std::vector<double> afinal_res;
	  problem.Evaluate(evopt,nullptr,&afinal_res,nullptr,nullptr);
	  int k = 0;
	  int kk = 0;
	  double norm_final_res = 0;
	  while(kk < afinal_res.size()-1) {
            double tmp = afinal_res[kk]*afinal_res[kk] + afinal_res[kk+1]*afinal_res[kk+1];
	    norm_final_res += weight(k)*tmp;
	    final_res(k) = sqrt(tmp)/abs(zeta_ref(k));
	    k += 1;
	    kk += 2;
	  }
	  if (flags.contains("misfit_val")) {
	    flags["misfit_val"] = 0.5*norm_final_res;
	  }
	}

        Lone_guess.setZero();
        Ltwo_guess.setZero();

        Lone_guess.real()(0,0) = S_II(0);
        Lone_guess.imag()(0,0) = S_II(1);
        Ltwo_guess.real()(0,0) = S_II(2);
        Ltwo_guess.imag()(0,0) = S_II(3);

	if ( N > 1) {
         
          for(int i = 1; i < N; i++){
		
	    Lone_guess.real()(0,i) = S_IE(i-1);
	    Lone_guess.imag()(0,i) = S_IE(i-1 + N-1);
	    Ltwo_guess.real()(0,i) = S_IE(i-1 +2*(N-1)); 
	    Ltwo_guess.imag()(0,i) = S_IE(i-1 +3*(N-1)); 

	    Lone_guess.real()(i,0) = S_EI(i-1); 
	    Lone_guess.imag()(i,0) = S_EI(i-1 + N-1); 
	    Ltwo_guess(i,0) = Complex(1,0); 

	    Lone_guess.real()(i,i) = S_EE(i-1);
	    Lone_guess.imag()(i,i) = S_EE(i-1 + (N-1));
	    Ltwo_guess(i,i) = Complex(1,0);

	  }
	}
    }

    void Run_mediumSym( Eigen::Ref<ComplexMatrix> Lone_guess,
	                Eigen::Ref<ComplexMatrix> Ltwo_guess,
			pybind11::dict flags,
		        Eigen::Ref<RealVector> final_res
		      ) { 

	int N = Lone_guess.cols();
	
	// ordering of vectors:
	// S_II = [Re(Lone),Im(Lone),Re(Ltwo),Im(Ltwo)] 
	
	RealVector S_II(4);
        
	S_II(0) = Lone_guess.real()(0,0);
        S_II(1) = Lone_guess.imag()(0,0);
        S_II(2) = Ltwo_guess.real()(0,0);
        S_II(3) = Ltwo_guess.imag()(0,0);

	RealVector S_IE;
	RealVector S_EE;

        if ( N > 1) {
	  
	  S_IE.resize(4*(N-1));
	  S_EE.resize(2*(N-1));
	  
	  for(int i=1; i <N; i++) {

	    S_IE(i-1)           =  Lone_guess.real()(0,i);
	    S_IE(i-1 + N-1)     =  Lone_guess.imag()(0,i);
	    S_IE(i-1 + 2*(N-1)) =  Ltwo_guess.real()(0,i);
	    S_IE(i-1 + 3*(N-1)) =  Ltwo_guess.imag()(0,i);

	    S_EE(i-1)           =  Lone_guess.real()(i,i);
	    S_EE(i-1 + N-1 )    =  Lone_guess.imag()(i,i);
	  
	  }

	}	

	Problem problem; 
        for(int i = 0; i < lam.size(); i++) {	
	  CostFunction* cost_function =
	    new DtnCostFunction_mediumSym(N,lam[i],zeta_ref[i]); 
	  if (N > 1) {
	    problem.AddResidualBlock(cost_function,new ScaledLoss(nullptr,weight[i],TAKE_OWNERSHIP) , 
			             S_II.data(),S_IE.data(),S_EE.data() 
				    );
	  } 
	  else {
	    problem.AddResidualBlock(cost_function,new ScaledLoss(nullptr,weight[i],TAKE_OWNERSHIP),S_II.data());
	  }
	}
	Solver::Options options;
	SetSolverOptions(options,flags);
	Solver::Summary summary;
	Solve(options, &problem, &summary);


	if (flags.contains("report_level")) {
	  if (std::string(pybind11::str(flags["report_level"])) == std::string("Brief")){
	    std::cout << summary.BriefReport() << "\n";
	  }
	  if (std::string(pybind11::str(flags["report_level"])) == std::string("Full")){
	    std::cout << summary.FullReport() << "\n";
	  }
	  if (std::string(pybind11::str(flags["report_level"])) == std::string("solver_stats")){
	    if (flags.contains("solver_stats")) {
              std::cout << "iterations: " << summary.num_successful_steps + summary.num_unsuccessful_steps
		        << ", initial cost: " << summary.initial_cost 
			<< ", final cost: " << summary.final_cost 
			<< ", total time [s]: " << summary.total_time_in_seconds 
			<< std::endl;
	      flags["solver_stats"]["initial_cost"] = summary.initial_cost;
	      flags["solver_stats"]["final_cost"] = summary.final_cost;
	      flags["solver_stats"]["iterations"] = summary.num_successful_steps + summary.num_unsuccessful_steps;
	      flags["solver_stats"]["total_time_in_seconds"] = summary.total_time_in_seconds;
	    }
	  }
	}

	if( final_res.size() == lam.size()) {
          auto evopt = Problem::EvaluateOptions();
          evopt.apply_loss_function = false;
	  std::vector<double> afinal_res;
	  problem.Evaluate(evopt,nullptr,&afinal_res,nullptr,nullptr);
	  int k = 0;
	  int kk = 0;
	  double norm_final_res = 0;
	  while(kk < afinal_res.size()-1) {
            double tmp = afinal_res[kk]*afinal_res[kk] + afinal_res[kk+1]*afinal_res[kk+1];
	    norm_final_res += weight(k)*tmp;
	    final_res(k) = sqrt(tmp)/abs(zeta_ref(k));
	    k += 1;
	    kk += 2;
	  }
	  if (flags.contains("misfit_val")) {
	    flags["misfit_val"] = 0.5*norm_final_res;
	  }
	}

        Lone_guess.setZero();
        Ltwo_guess.setZero();

        Lone_guess.real()(0,0) = S_II(0);
        Lone_guess.imag()(0,0) = S_II(1);
        Ltwo_guess.real()(0,0) = S_II(2);
        Ltwo_guess.imag()(0,0) = S_II(3);

	if ( N > 1) {
         
          for(int i = 1; i < N; i++){
		
	    Lone_guess.real()(0,i) = S_IE(i-1);
	    Lone_guess.imag()(0,i) = S_IE(i-1 + N-1);
	    Lone_guess(i,0) = Lone_guess(0,i); 

	    Ltwo_guess.real()(0,i) = S_IE(i-1 +2*(N-1)); 
	    Ltwo_guess.imag()(0,i) = S_IE(i-1 +3*(N-1)); 
	    Ltwo_guess(i,0) = Ltwo_guess(0,i); 

	    Lone_guess.real()(i,i) = S_EE(i-1);
	    Lone_guess.imag()(i,i) = S_EE(i-1 + (N-1));
	    Ltwo_guess(i,i) = Complex(1,0);

	  }
	}
    }

    void Run_reduced( Eigen::Ref<ComplexMatrix> Lone_guess,
	              Eigen::Ref<ComplexMatrix> Ltwo_guess,
		      pybind11::dict flags,
		      Eigen::Ref<RealVector> final_res
		    ) { 

	int N = Lone_guess.cols();


	RealVector L1d_real;	
	RealVector L1d_imag;

        if ( N > 1) {
	  L1d_real.resize(N-1);	
	  L1d_imag.resize(N-1);
	  for (int j = 0; j < N-1; j++) {
            L1d_real(j) = Lone_guess(j+1,j+1).real();
            L1d_imag(j) = Lone_guess(j+1,j+1).imag();
	  }
	
	
	  Problem problem;          
	  CostFunction* cost_function;
       	  
	  if(pybind11::cast<std::string>(flags["PseudoType"]) == "SVD")  {
	    cost_function =
	      new DtnCostFunction_reduced(N,lam,zeta_ref,weight,pybind11::cast<std::string>(flags["PseudoType"]),pybind11::cast<double>(flags["sing_tol"]));
	  }
	  else {
	    cost_function =
	      new DtnCostFunction_reduced(N,lam,zeta_ref,weight,pybind11::cast<std::string>(flags["PseudoType"])); 
	  }

	  problem.AddResidualBlock(cost_function, NULL, 
			          L1d_real.data(), L1d_imag.data()
				 );

          std::cout << pybind11::cast<std::string>(flags["PseudoType"]) << std::endl;  
             	  
	  Solver::Options options;
   	  SetSolverOptions(options,flags);
	  Solver::Summary summary;
	  Solve(options, &problem, &summary);       

	  if (flags.contains("report_level")) {
	    if (std::string(pybind11::str(flags["report_level"])) == std::string("Brief")){
	      std::cout << summary.BriefReport() << "\n";
	    }
	  if (std::string(pybind11::str(flags["report_level"])) == std::string("Full")){
	    std::cout << summary.FullReport() << "\n";
	    }
	  }

	  if( final_res.size() == lam.size()) {
            auto evopt = Problem::EvaluateOptions();
            // evopt.apply_loss_function = false;
	    std::vector<double> afinal_res;
	    problem.Evaluate(evopt,nullptr,&afinal_res,nullptr,nullptr);
	    
	    for( int l = 0; l < lam.size(); l++) {
	      final_res(l) = std::sqrt( afinal_res[l]*afinal_res[l] + afinal_res[l+lam.size()]*afinal_res[l+lam.size()]  )/(weight(l)*std::abs(zeta_ref(l))); 
	    }
	  }
      
	} 


        Lone_guess.setZero();
        Ltwo_guess.setZero();
	for (int j=0; j < N-1; j++) {
	  Lone_guess(j+1,j+1) = Complex(L1d_real(j),L1d_imag(j));
	  Ltwo_guess(j+1,j+1) = Complex(1.0,0.0);
	}

        // Least squares problem for linear variables

        ComplexMatrix A(lam.size(),N+1);
        
        for (int l = 0; l < lam.size(); l++) {
          A(l,0) = weight(l);
          A(l,1) = weight(l)*lam(l);
          for (int j= 0; j < N-1; j++) {
            A(l,2+j) = -weight(l)/( Lone_guess(j+1,j+1) +lam(l) );
          } 
        }
        ComplexVector b = zeta_ref;
        for(int l = 0; l < b.size(); l++) {
          b(l) *= weight(l);
        }

        // ComplexMatrix A_dagger = A.completeOrthogonalDecomposition().pseudoInverse();
        /* ComplexMatrix AH_A = A.adjoint() * A; 
	ComplexMatrix AH_A_inv = AH_A.inverse();
        ComplexMatrix A_dagger = AH_A_inv * A.adjoint(); */
	
	// ComplexVector x_linear = A.completeOrthogonalDecomposition().solve(b); 
	
	//ComplexVector x_linear = A.fullPivHouseholderQr().solve(b); 
	
	
	ComplexVector x_linear; 
        if(pybind11::cast<std::string>(flags["PseudoType"]) == "COD_weighted") {
          
	 Eigen::ColPivHouseholderQR<ComplexMatrix> AH_QR(A.adjoint().rows(),A.adjoint().cols());
	 AH_QR.compute(A.adjoint());   
	 ComplexMatrix RH_AH = AH_QR.matrixR().triangularView<Eigen::Upper>().adjoint();
	 Eigen::HouseholderQR<ComplexMatrix> RH_AH_QR(RH_AH.rows(),RH_AH.cols());
	 RH_AH_QR.compute(RH_AH);
         x_linear =  Solve_weighted(b,AH_QR,RH_AH_QR);
	}
        else {
	  x_linear = A.fullPivHouseholderQr().solve(b); 
	}	
	

	// ComplexVector x_linear = A_dagger * b; 
        Lone_guess(0,0) = x_linear(0);
        Ltwo_guess(0,0) = x_linear(1);
	for (int j = 0; j < N-1; j++) {
	  Lone_guess(j+1,0) = std::sqrt(x_linear(j+2));	
	  Lone_guess(0,j+1) = std::sqrt(x_linear(j+2));	
	}
    
    }

    void Run_minalg( Eigen::Ref<ComplexMatrix> Lone_guess,
	              Eigen::Ref<ComplexMatrix> Ltwo_guess,
		      pybind11::dict flags,
		      Eigen::Ref<RealVector> final_res
		   ) { 

	int N = Lone_guess.cols();


	RealVector L1d_real;	
	RealVector L1d_imag;

        if ( N > 1) {

	  L1d_real.resize(N-1);	
	  L1d_imag.resize(N-1);
	  for (int j = 0; j < N-1; j++) {
            L1d_real(j) = Lone_guess(j+1,j+1).real();
            L1d_imag(j) = Lone_guess(j+1,j+1).imag();
	  }
	
	   
	   pybind11::list sigma_iter = pybind11::cast<pybind11::list>(flags["sigma_iter"]);
	   pybind11::list cost_iter = pybind11::cast<pybind11::list>(flags["cost_iter"]);
	   // sigma_iter.append(99);

	   double smallest_sigma = 99;
	   double sigma_tol = pybind11::cast<double>(flags["sing_tol"]);
	   
	   Solver::Options options;
   	   SetSolverOptions(options,flags);
	   options.update_state_every_iteration = true;   

	   Problem problem;          
	   CostFunction* cost_function;
       	  
	   if(pybind11::cast<std::string>(flags["PseudoType"]) == "SVD")  {
	     cost_function =
	       new DtnCostFunction_reduced(N,lam,zeta_ref,weight,
			                  pybind11::cast<std::string>(flags["PseudoType"]),
					  pybind11::cast<double>(flags["sing_tol"]),
			                  &smallest_sigma);
	   }
	   else {
	     cost_function =
	       new DtnCostFunction_reduced(N,lam,zeta_ref,weight,
			                  pybind11::cast<std::string>(flags["PseudoType"]),
					  &smallest_sigma); 
	   }
 
  	   problem.AddResidualBlock(cost_function, NULL, 
	 		            L1d_real.data(), L1d_imag.data()
				   );

           std::cout << pybind11::cast<std::string>(flags["PseudoType"]) << std::endl;  
             	  
           // BoolIterationCallback( double a_sigma_tol,double* a_smallest_sigma, pybind11::list * a_sigma_iter ) {
           BoolIterationCallback callback0(sigma_tol,&smallest_sigma,&sigma_iter,&cost_iter); 
	   options.callbacks.push_back(&callback0); 
	   Solver::Summary summary;
	   
           ComplexVector b_rhs = zeta_ref;
           for(int l = 0; l < b_rhs.size(); l++) {
             b_rhs(l) *= weight(l);
           }
	   
           ComplexMatrix Az(lam.size(),N+1);

	   int max_iter = 4; 
	   int iter = 0; 
         
	   while( iter < max_iter &&  ! summary.IsSolutionUsable() ) {
             std::cout << "a" << std::endl; 
	     if (smallest_sigma < sigma_tol) {

	       int iter_current = pybind11::len(pybind11::cast<pybind11::list>(flags["cost_iter"]))-1 ;
	       pybind11::cast<pybind11::list>(flags["search_iter"]).append(iter_current);

	       std::cout << "b" << std::endl; 
	        
               for (int l = 0; l < lam.size(); l++) {
                 Az(l,0) = weight(l);
                 Az(l,1) = weight(l)*lam(l);
                 for (int j= 0; j < N-1; j++) {
                   Az(l,2+j) = -weight(l)/( Complex(L1d_real(j),L1d_imag(j)) +lam(l) );
                 } 
               }

	      // Complex new_pole =  Maximize_by_sampling_direct(b_rhs,Az,lam,weight,flags);
	      RealMatrix fun_vals( (pybind11::cast<Eigen::Ref<RealVector>>(flags["x_sample"])).rows(),
			           ( pybind11::cast<Eigen::Ref<RealVector>>(flags["y_sample"])).rows() );
	      
	      Complex new_pole =  Maximize_by_sampling_direct(b_rhs,Az,lam,weight,flags,fun_vals); 
	      // Complex new_pole =  Maximize_by_sampling_LQ(b_rhs,Az,lam,weight,flags,fun_vals); 
	      
	      pybind11::cast<pybind11::list>(flags["obj_fun"]).append(fun_vals);
	      RealVector obj_fun_val(N-1);
	      RealVector A_pr_sing(N-1);

	      for(int j=0; j < N-1; j++) {
	        ComplexMatrix A_with_pole_replaced = Az;
	        for(int l= 0; l < lam.size(); l++) {
	          A_with_pole_replaced(l,2+j) = -weight(l)/( new_pole + lam(l) );
	        }
	        auto A_pr_svd = A_with_pole_replaced.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV);
                A_pr_sing(j) = A_pr_svd.singularValues()( A_pr_svd.singularValues().size()-1); 
                auto A_pr_QR = A_with_pole_replaced.completeOrthogonalDecomposition();
                obj_fun_val(j) = std::pow((A_with_pole_replaced * A_pr_QR.solve(b_rhs) - b_rhs).norm(),2); 
	      }

              int idx_min; 
              double min_val = obj_fun_val.minCoeff(&idx_min);
	    
              int idx_max; 
              double max_val = A_pr_sing.maxCoeff(&idx_max);
	    
	      std::cout << "obj_fun_val = " << obj_fun_val << std::endl; 
	      std::cout << "A_pr_sing = " << A_pr_sing << std::endl; 
	      std::cout << "idx_min = " << idx_min << ", min_val = " << min_val << std::endl; 
	      std::cout << "idx_max = " << idx_max << ", max_val = " << max_val << std::endl; 

              int chosen_idx = idx_max;
	      for (int j =0; j< N-1;j++) {
	        if ( (A_pr_sing(j) > pybind11::cast<double>(flags["sing_tol"])) && obj_fun_val(j) < obj_fun_val(chosen_idx)) {
		  chosen_idx  = j; 
		}	
	      }

	      if( pybind11::cast<bool>(flags["smallest_cost"])  ) {
	        chosen_idx = idx_min;
	      }
	      //chosen_idx = idx_max;
	      std::cout << "chosen_idx  = " << chosen_idx << std::endl; 
	      L1d_real(chosen_idx) = new_pole.real();
              L1d_imag(chosen_idx) = new_pole.imag();
              
	      // callback0.SetSingTol(pybind11::cast<double>(flags["sing_tol"])/10);
              iter++; 
	     }

             	     
	     Solve(options, &problem, &summary);       

	     std::cout << "Here we are" << std::endl; 

	     if (flags.contains("report_level")) {
	       if (std::string(pybind11::str(flags["report_level"])) == std::string("Brief")){
	         std::cout << summary.BriefReport() << "\n";
	       }
	     if (std::string(pybind11::str(flags["report_level"])) == std::string("Full")){
	       std::cout << summary.FullReport() << "\n";
	       }
	     }

	   }

	   if( final_res.size() == lam.size()) {
             auto evopt = Problem::EvaluateOptions();
             // evopt.apply_loss_function = false;
	     std::vector<double> afinal_res;
	     problem.Evaluate(evopt,nullptr,&afinal_res,nullptr,nullptr);
	    
	     for( int l = 0; l < lam.size(); l++) {
	       final_res(l) = std::sqrt( afinal_res[l]*afinal_res[l] + afinal_res[l+lam.size()]*afinal_res[l+lam.size()]  )/(weight(l)*std::abs(zeta_ref(l))); 
	     }
	   } 
           
	   Lone_guess.setZero();
           Ltwo_guess.setZero();
	   for (int j=0; j < N-1; j++) {
	     Lone_guess(j+1,j+1) = Complex(L1d_real(j),L1d_imag(j));
	     Ltwo_guess(j+1,j+1) = Complex(1.0,0.0);
	   }
           
	  
	 // }

       }

        // Least squares problem for linear variables

        ComplexMatrix A(lam.size(),N+1);
        
        for (int l = 0; l < lam.size(); l++) {
          A(l,0) = weight(l);
          A(l,1) = weight(l)*lam(l);
          for (int j= 0; j < N-1; j++) {
            A(l,2+j) = -weight(l)/( Lone_guess(j+1,j+1) +lam(l) );
          } 
        }
        ComplexVector b = zeta_ref;
        for(int l = 0; l < b.size(); l++) {
          b(l) *= weight(l);
        }

        // ComplexMatrix A_dagger = A.completeOrthogonalDecomposition().pseudoInverse();
        /* ComplexMatrix AH_A = A.adjoint() * A; 
	ComplexMatrix AH_A_inv = AH_A.inverse();
        ComplexMatrix A_dagger = AH_A_inv * A.adjoint(); */
	
	// ComplexVector x_linear = A.completeOrthogonalDecomposition().solve(b); 
	
	//ComplexVector x_linear = A.fullPivHouseholderQr().solve(b); 
	
	
	ComplexVector x_linear; 
        if(pybind11::cast<std::string>(flags["PseudoType"]) == "COD_weighted") {
          
	 Eigen::ColPivHouseholderQR<ComplexMatrix> AH_QR(A.adjoint().rows(),A.adjoint().cols());
	 AH_QR.compute(A.adjoint());   
	 ComplexMatrix RH_AH = AH_QR.matrixR().triangularView<Eigen::Upper>().adjoint();
	 Eigen::HouseholderQR<ComplexMatrix> RH_AH_QR(RH_AH.rows(),RH_AH.cols());
	 RH_AH_QR.compute(RH_AH);
         x_linear =  Solve_weighted(b,AH_QR,RH_AH_QR);
	}
        else {
	  x_linear = A.fullPivHouseholderQr().solve(b); 
	}	
	

	// ComplexVector x_linear = A_dagger * b; 
        Lone_guess(0,0) = x_linear(0);
        Ltwo_guess(0,0) = x_linear(1);
	for (int j = 0; j < N-1; j++) {
	  Lone_guess(j+1,0) = std::sqrt(x_linear(j+2));	
	  Lone_guess(0,j+1) = std::sqrt(x_linear(j+2));	
	}
    
    }

}; 



void eval_dtn_fct(Eigen::Ref<ComplexMatrix> Lone,
	          Eigen::Ref<ComplexMatrix> Ltwo,
		  Eigen::Ref<RealVector> lam,
		  Eigen::Ref<ComplexVector> val,
		  bool calc_inverse
		) 
{
  int N = Lone.cols();
  
  ComplexMatrix S(N,N); 
  ComplexMatrix SE;
  ComplexMatrix SE_inv;
  
  if(N > 1) { 
    SE.resize(N-1,N-1);
  }

  for(int k=0; k < lam.size(); k++) {
 
    for (int i = 0; i < N; i++){
      for(int j=  0; j < N; j++){
        S(i,j) = Lone(i,j) + lam(k)*Ltwo(i,j);
	if (N > 1 && i > 0 && j > 0) {
	  SE(i-1,j-1) = S(i,j);
	}
      }
    }
    
    Complex zeta_approx = S(0,0);
    
    if( N > 1) {

      if (calc_inverse) {     
        SE_inv = SE.inverse(); 
        for (int i=1;i<N;i++){
	  for(int j=1;j<N;j++){
	    zeta_approx -= S(0,i)*SE_inv(i-1,j-1)*S(j,0);
	  }
	}
      }
      else {
        ComplexVector SEG(N-1);
        for (int j = 0; j < N-1; j++) {
	  SEG(j) = S(j+1,0);
	}
	ComplexVector res  = SE.colPivHouseholderQr().solve(SEG);
	for (int j=1;j<N;j++){
	  zeta_approx -= S(0,j)*res(j-1);
	}
      }
	    
    }
    
    val(k) = zeta_approx;	
      
  }
  
     
}


void eval_dtn_fct_interval(Eigen::Ref<ComplexMatrix> Aone,
	                   Eigen::Ref<ComplexMatrix> Atwo,
	                   Eigen::Ref<ComplexMatrix> B,
		           Eigen::Ref<RealVector> lam,
		           Eigen::Ref<ComplexVector> g_omega,
		           Eigen::Ref<ComplexMatrix> val
		          ) 
{
  int N = Aone.cols();
  
  ComplexMatrix S(N,N); 
  ComplexMatrix SE;
  ComplexMatrix SE_inv;
      
  if(N > 1) { 
    SE.resize(N-1,N-1);
  }       
  Complex diff = Complex(0.0,0.0);
  
   
  for(int l = 0; l < lam.size(); l++) {
    for(int k = 0; k < g_omega.size(); k++) {
      diff = Complex(0.0,0.0);  
      for (int i = 0; i < N; i++){
        for(int j=  0; j < N; j++){
          S(i,j) = Aone(i,j) + g_omega(k)*Atwo(i,j) + lam(l)*B(i,j);	   
          if (N > 1 && i > 0 && j > 0) { 
            SE(i-1,j-1) = S(i,j);
          }
        }
      }
      if( N > 1) {
        SE_inv = SE.inverse();
      }
      Complex dtn_approx = S(0,0);
      if (N > 1) {
        for (int i=1;i<N;i++){
          for(int j=1;j<N;j++){
            dtn_approx -= S(0,i)*SE_inv(i-1,j-1)*S(j,0);
	  }
	}
      }
      val(l,k) = dtn_approx; 
    }
  }      
}


class learned_dtn_interval
{
  protected:

  public:
    RealVector lam; // eigenvalues
    ComplexVector g_omega; // g(omega) with omega = wavenumber
    ComplexMatrix dtn_ref; // dtn numbers 
    RealMatrix weight; // weights
    
    learned_dtn_interval(RealVector alam,ComplexVector ag_omega,ComplexMatrix adtn_ref,RealMatrix aweight)  { 
      lam = alam; 
      g_omega = ag_omega; 
      dtn_ref = adtn_ref;     
      weight = aweight;    
    }

    void Run_full( Eigen::Ref<ComplexMatrix> Aone_guess,
	           Eigen::Ref<ComplexMatrix> Atwo_guess, 
	           Eigen::Ref<ComplexMatrix> B_guess,
		   pybind11::dict flags
                 ) { 

	int N = Aone_guess.cols();
	
	RealVector A1_real(N*N);
	RealVector A1_imag(N*N);
	RealVector A2_real(N*N);
	RealVector A2_imag(N*N);
	RealVector B_real(N*N);
	RealVector B_imag(N*N);

        for (int i = 0; i < N; i++) {
          for(int j= 0; j < N; j++) {
	    A1_real(j+i*N) = Aone_guess.real()(i,j);
	    A1_imag(j+i*N) = Aone_guess.imag()(i,j);
	    A2_real(j+i*N) = Atwo_guess.real()(i,j);
	    A2_imag(j+i*N) = Atwo_guess.imag()(i,j);
	    B_real(j+i*N) = B_guess.real()(i,j);
	    B_imag(j+i*N) = B_guess.imag()(i,j);
	  }
	}


	
	Problem problem; 
        
	for(int i = 0; i < lam.size(); i++) {
	  for(int j = 0; j < weight.cols();j++) {
	    CostFunction* cost_function =
	      new DtnCostFunction_full_interval(N,lam(i),g_omega(j),dtn_ref(i,j)); 
	      problem.AddResidualBlock(cost_function, 
			               new ScaledLoss(nullptr,weight(i,j),TAKE_OWNERSHIP), 
				       A1_real.data(),A1_imag.data(),B_real.data(),B_imag.data(),A2_real.data(),A2_imag.data());
	  }
	}
	Solver::Options options;
	SetSolverOptions(options,flags);
	Solver::Summary summary;
	Solve(options, &problem, &summary);

	if (flags.contains("report_level")) {
	  if (std::string(pybind11::str(flags["report_level"])) == std::string("Brief")){
	    std::cout << summary.BriefReport() << "\n";
	  }
	  if (std::string(pybind11::str(flags["report_level"])) == std::string("Full")){
	    std::cout << summary.FullReport() << "\n";
	  }
	}
 
	for (int i = 0; i < N; i++) {
          for(int j= 0; j < N; j++) {
	    Aone_guess(i,j) = Complex(A1_real(j+i*N),A1_imag(j+i*N));
	    Atwo_guess(i,j) = Complex(A2_real(j+i*N),A2_imag(j+i*N));
	    B_guess(i,j)    = Complex(B_real(j+i*N),B_imag(j+i*N));
	  }
	} 
    }
    
    void Run_sym(  Eigen::Ref<ComplexMatrix> Aone_guess,
	           Eigen::Ref<ComplexMatrix> Atwo_guess, 
	           Eigen::Ref<ComplexMatrix> B_guess,
		   pybind11::dict flags
                 ) { 

	int N = Aone_guess.cols();
	int N_var = N*(N+1)/2; 

	RealVector A1_real(N_var);
	RealVector A1_imag(N_var);
	RealVector A2_real(N_var);
	RealVector A2_imag(N_var);
	RealVector B_real(N_var);
	RealVector B_imag(N_var);

        for (int i = 0; i < N; i++) {
          for(int j= i; j < N; j++) {
	    int idx_lin  = (N*(N-1)/2) - int((N-i)*(N-i-1)/2) + j;
            A1_real(idx_lin) = Aone_guess.real()(i,j);
	    A1_imag(idx_lin) = Aone_guess.imag()(i,j);
	    A2_real(idx_lin) = Atwo_guess.real()(i,j);
	    A2_imag(idx_lin) = Atwo_guess.imag()(i,j);
	    B_real(idx_lin) = B_guess.real()(i,j);
	    B_imag(idx_lin) = B_guess.imag()(i,j);
	  }
	}

	Problem problem; 
        
	for(int i = 0; i < lam.size(); i++) {
	  for(int j = 0; j < weight.cols();j++) {
	    CostFunction* cost_function =
	      new DtnCostFunction_sym_interval(N,lam(i),g_omega(j),dtn_ref(i,j)); 
	      problem.AddResidualBlock(cost_function, 
			               new ScaledLoss(nullptr,weight(i,j),TAKE_OWNERSHIP), 
				       A1_real.data(),A1_imag.data(),B_real.data(),B_imag.data(),A2_real.data(),A2_imag.data());
	  }
	}
	Solver::Options options;
	SetSolverOptions(options,flags);
	Solver::Summary summary;
	Solve(options, &problem, &summary);

	if (flags.contains("report_level")) {
	  if (std::string(pybind11::str(flags["report_level"])) == std::string("Brief")){
	    std::cout << summary.BriefReport() << "\n";
	  }
	  if (std::string(pybind11::str(flags["report_level"])) == std::string("Full")){
	    std::cout << summary.FullReport() << "\n";
	  }
	  if (std::string(pybind11::str(flags["report_level"])) == std::string("solver_stats")){
	    if (flags.contains("solver_stats")) {
              std::cout << "iterations: " << summary.num_successful_steps + summary.num_unsuccessful_steps
		        << ", initial cost: " << summary.initial_cost 
			<< ", final cost: " << summary.final_cost 
			<< ", total time [s]: " << summary.total_time_in_seconds 
			<< std::endl;
	      flags["solver_stats"]["initial_cost"] = summary.initial_cost;
	      flags["solver_stats"]["final_cost"] = summary.final_cost;
	      flags["solver_stats"]["iterations"] = summary.num_successful_steps + summary.num_unsuccessful_steps;
	      flags["solver_stats"]["total_time_in_seconds"] = summary.total_time_in_seconds;
	    }
	  }
	}
 
	for (int i = 0; i < N; i++) {
          for(int j= i; j < N ; j++) {
	    int idx_lin  = (N*(N-1)/2) - int((N-i)*(N-i-1)/2) + j;
	    Aone_guess(i,j) = Complex(A1_real(idx_lin),A1_imag(idx_lin));
	    Atwo_guess(i,j) = Complex(A2_real(idx_lin),A2_imag(idx_lin));
	    B_guess(i,j)    = Complex(B_real(idx_lin),B_imag(idx_lin));
	  }
	} 
	for (int i = 0; i < N; i++) {
          for(int j= 0; j < i; j++) {
	    Aone_guess(i,j) =  Aone_guess(j,i);
	    Atwo_guess(i,j) =  Atwo_guess(j,i); 
	    B_guess(i,j)    =  B_guess(j,i);
	  }
	} 
    }
};




namespace py = pybind11;

PYBIND11_MODULE(ceres_dtn, m) {
    m.doc() = R"pbdoc(
        Pybind11 example plugin
        -----------------------
        .. currentmodule:: cmake_example
        .. autosummary::
           :toctree: _generate
    )pbdoc";

    m.def("solve_problem", &solve_problem, R"pbdoc(
        Some doctring
    )pbdoc");

    py::class_<learned_dtn, shared_ptr<learned_dtn>>
      (m,"learned_dtn","Learned DtN")
      //.def(py::init<int,int>())
      .def(py::init<RealVector,ComplexVector,RealVector>())
      .def("Run",[] (learned_dtn self,
		     Eigen::Ref<ComplexMatrix> Lone,
		     Eigen::Ref<ComplexMatrix> Ltwo,
		     const std::string & ansatz,
		     py::dict flags,
		     Eigen::Ref<RealVector> final_res
			      ) {
	if( Lone.rows() != Lone.cols()) {
	  throw std::invalid_argument("Lone has to be a quadratic matrix!");
	}
        
	py::scoped_ostream_redirect stream(
	  std::cout,
	  py::module::import("sys").attr("stdout")
	);
        	
	if ( ansatz == "minimal") {
	   self.Run_minimal(Lone,Ltwo,flags,final_res);
	}
	else {
	      if ( ansatz  == "medium" ) {
	        self.Run_medium(Lone,Ltwo,flags,final_res);
	      }
	      else {
	            if ( ansatz == "minimalIC") {
	              self.Run_minimalIC(Lone,Ltwo,flags,final_res);
		    } 
	            else { 
		         if ( ansatz == "reduced" ) {	   
	                   self.Run_reduced(Lone,Ltwo,flags,final_res);
			 } 
			 else {
			      if  ( ansatz == "minalg") {
	                        self.Run_minalg(Lone,Ltwo,flags,final_res);
			      }
			      else {
		                   if ( ansatz == "mediumSym") {
	                             self.Run_mediumSym(Lone,Ltwo,flags,final_res);
				   }
	                           else {
	                             self.Run_full(Lone,Ltwo,flags,final_res);
				   }			   
			      } 
			 }	   
	            }
	      }
	}
      
      },
      py::arg("Lone"),py::arg("Ltwo"),py::arg("ansatz")="full",
      py::arg("flags")=py::dict(),
      py::arg("final_res")=RealVector(1) )
      ;

    
    py::class_<learned_dtn_interval, shared_ptr<learned_dtn_interval>>
      (m,"learned_dtn_interval","Learned DtN interval")
      //.def(py::init<int,int>())
      .def(py::init<RealVector, //lambda
		    ComplexVector, //g_omega
		    ComplexMatrix, //dtn_ref_omega
		    RealMatrix // weight_omega 
		    >())
      .def("Run",[] (learned_dtn_interval self,
		     Eigen::Ref<ComplexMatrix> Aone,
		     Eigen::Ref<ComplexMatrix> Atwo,
		     Eigen::Ref<ComplexMatrix> B,
		     const std::string & ansatz,
		     py::dict flags
			      ) {
	if( Aone.rows() != Aone.cols()) {
	  throw std::invalid_argument("Aone has to be a quadratic matrix!");
	}
        
	py::scoped_ostream_redirect stream(
	  std::cout,
	  py::module::import("sys").attr("stdout")
	);
        	
	if ( ansatz == "symmetric") {
           self.Run_sym(Aone,Atwo,B,flags);
	}
	else {
           self.Run_full(Aone,Atwo,B,flags);
	}
      
      },
      py::arg("Aone"),py::arg("Atwo"),py::arg("B"),py::arg("ansatz")="full",
      py::arg("flags")=py::dict() )
      ;
      

      m.def("eval_dtn_fct",[] ( Eigen::Ref<ComplexMatrix> Lone,
		                Eigen::Ref<ComplexMatrix> Ltwo,
				Eigen::Ref<RealVector> lam,
				Eigen::Ref<ComplexVector> val,
				bool calc_inverse
			       ) {
        eval_dtn_fct(Lone,Ltwo,lam,val,calc_inverse);
      }, 
      py::arg("Lone"),py::arg("Ltwo"),py::arg("lam"),py::arg("val"),py::arg("calc_inverse")=true
      ); 
      
      m.def("eval_dtn_fct_interval",[] (Eigen::Ref<ComplexMatrix> Aone,
	                                Eigen::Ref<ComplexMatrix> Atwo,
	                                Eigen::Ref<ComplexMatrix> B,
		                        Eigen::Ref<RealVector> lam,
		                        Eigen::Ref<ComplexVector> g_omega,
		                        Eigen::Ref<ComplexMatrix> val
			       ) {
        eval_dtn_fct_interval(Aone,Atwo,B,lam,g_omega,val);
      }, 
      py::arg("Aone"),py::arg("Atwo"),py::arg("B"),py::arg("lam"),py::arg("g_omega"),py::arg("val")
      ); 

#ifdef VERSION_INFO
    m.attr("__version__") = VERSION_INFO;
#else
    m.attr("__version__") = "dev";
#endif
}

