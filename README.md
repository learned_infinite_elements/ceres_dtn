# ceres_dtn

Solution of the minimization problem for the learned DtN using the ceres-solver.

## Dependencies 
* cmake
* [ceres-solver](http://ceres-solver.org/index.html)
* python3, pybind11, NumPy, (SciPy) 

## Installation 
* `mkdir build`
* `cd build`
* `cmake ..`

## Test
* `cd tests`
* `python3 helmholtz2d.py`

